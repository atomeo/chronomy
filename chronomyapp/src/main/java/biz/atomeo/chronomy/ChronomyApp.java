package biz.atomeo.chronomy;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;

import com.google.android.gms.ads.MobileAds;

import java.util.UUID;

import biz.atomeo.chronomy.storage.Preferences;
import biz.atomeo.chronomy.util.DateTime;

/**
 * Created by pavelstakhov on 12.06.18.
 */

public class ChronomyApp extends MultiDexApplication {

    private static ChronomyApp mApplication;
    private String uniqueID;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = this;

        initUniqueID();

        initStrings();
        MobileAds.initialize(this, getString(R.string.chronomy_admob_app_id));
    }

    public void initUniqueID() {
        uniqueID=Preferences.getUUID();
        if (uniqueID==null) {
            uniqueID = UUID.randomUUID().toString();
            Preferences.saveUUID(uniqueID);
        }
    }

    private void initStrings() {
        DateTime.setup(
                mApplication.getString(R.string.year1),
                mApplication.getString(R.string.year2),
                mApplication.getString(R.string.year3),

                mApplication.getString(R.string.month1),
                mApplication.getString(R.string.month2),
                mApplication.getString(R.string.month3),

                mApplication.getString(R.string.week1),
                mApplication.getString(R.string.week2),
                mApplication.getString(R.string.week3),

                mApplication.getString(R.string.day1),
                mApplication.getString(R.string.day2),
                mApplication.getString(R.string.day3),

                mApplication.getString(R.string.hour1),
                mApplication.getString(R.string.hour2),
                mApplication.getString(R.string.hour3),

                mApplication.getString(R.string.minute1),
                mApplication.getString(R.string.minute2),
                mApplication.getString(R.string.minute3),

                mApplication.getString(R.string.rightNow),
                mApplication.getString(R.string.beforeEvent),
                mApplication.getString(R.string.ago),
                "Y".equals(mApplication.getString(R.string.russianDigits))
        );
    }

    public static ChronomyApp getApplication() {
        return mApplication;
    }


    public final String getUUID() {
        return uniqueID;
    }
}
