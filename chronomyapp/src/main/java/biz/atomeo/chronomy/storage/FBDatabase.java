package biz.atomeo.chronomy.storage;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pavelstakhov on 14.06.18.
 */

public class FBDatabase {

    private static final String USERS = "users";

    private static final String NOTES = "notes";
    private static final String ARCHIVE = "archive";

    private static final String PROFILE = "profile";
    private static final String HISTORY = "history";
    private static final String DISPLAY_NAME = "displayName";
    private static final String LOGGED_IN_DEVICE_ID = "loggedInDeviceId";
    private static final String PAID_UNTIL = "paidUntil";
    private static final String EMAIL = "email";

    private static final String GROUPS = "groups";

    private static FBDatabase instance;

    private class MemorizedChildEventListener {
        private ChildEventListener listener;
        private Query query;
        private Object whoKey;

        public MemorizedChildEventListener(Object whoKey, ChildEventListener listener, Query query) {
            this.listener = listener;
            this.query = query;
            this.whoKey = whoKey;
        }

        public ChildEventListener getListener() {
            return listener;
        }

        public void setListener(ChildEventListener listener) {
            this.listener = listener;
        }

        public Query getQuery() {
            return query;
        }

        public void setQuery(Query query) {
            this.query = query;
        }

        public Object getWhoKey() {
            return whoKey;
        }

        public void setWhoKey(Object whoKey) {
            this.whoKey = whoKey;
        }
    }

    private ArrayList<MemorizedChildEventListener> memorizedChildEventListener = new ArrayList<>();

    private static final Object monitor = new Object();

    public static FBDatabase getInstance() {
        if (instance==null) {
            synchronized (monitor) {
                instance = new FBDatabase();
                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                if (user!=null) {
                    String uid = user.getUid();
                    FirebaseDatabase.getInstance().getReference()
                            .child(USERS)
                            .child(uid)
                            .keepSynced(true);
                }

            }
        }
        return instance;
    }

    private FBDatabase() {

    }

    public boolean isUserLoggedIn() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        return user!=null;
    }

    public interface AfterLoginListener {
        void loginCompleted();
        void loginError();
    }

    public void afterLogin(final AfterLoginListener listener, final String deviceId) {
        if (!isUserLoggedIn()) throw new SecurityException();
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase.getInstance().getReference()
                .child(USERS)
                .child(uid)
                .child(PROFILE).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                        if (user!=null) {
                            String uid = user.getUid();
                            DatabaseReference profileReference = FirebaseDatabase.getInstance().getReference()
                                    .child(USERS).child(uid).child(PROFILE);
                            if (!dataSnapshot.exists()) {
                                String displayName = user.getDisplayName();
                                String email = user.getEmail();
                                profileReference.child(DISPLAY_NAME).setValue(displayName);
                                profileReference.child(EMAIL).setValue(email);
                            } else {
                                //TODO: проверка платной версии и преднастройка приложения

                            }
                            profileReference.child(LOGGED_IN_DEVICE_ID).setValue(deviceId);
                            listener.loginCompleted();
                        } else listener.loginError();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        listener.loginError();
                    }
                });

    }

    public interface OnLoadLoggedDevice {
        void loggedUUIDLoaded(String uuid);
    }

    public void loadLoggedDevice(final OnLoadLoggedDevice listener) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase.getInstance().getReference()
                .child(USERS).child(uid).child(PROFILE).child(LOGGED_IN_DEVICE_ID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    try {
                        String loggedUUID = (String) dataSnapshot.getValue();
                        listener.loggedUUIDLoaded(loggedUUID);
                    } catch (Exception e) {
                        //TODO:
                    }
                } else {
                    listener.loggedUUIDLoaded("");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public interface OnLoadProfile {
        void profileLoaded(ProfileModel profile);
        void profileNotLoggedIn();
    }

    public void loadProfile(final OnLoadProfile listener) {
        if (!isUserLoggedIn()) {
            listener.profileNotLoggedIn();
            return;
        }

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user!=null) {
            String uid = user.getUid();
            FirebaseDatabase.getInstance().getReference()
                .child(USERS).child(uid).child(PROFILE).child(PAID_UNTIL)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        if (user!=null) {
                            String displayName = user.getDisplayName();
                            String email = user.getEmail();
                            String paidUntil = (String) dataSnapshot.getValue();
                            boolean premiumAccount = biz.atomeo.chronomy.util.DateTime.isDateInFuture(paidUntil);
                            if (listener != null)
                                listener.profileLoaded(new ProfileModel(displayName, email, premiumAccount, paidUntil));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

        }
    }

    public void saveEndOfPrepaidPeriod(String timestamp) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String uid = user.getUid();
        DatabaseReference profileReference = FirebaseDatabase.getInstance().getReference()
                .child(USERS).child(uid).child(PROFILE);
        profileReference.child(PAID_UNTIL).setValue(timestamp);
    }

    public NoteModel createValueNote(String name, String description, String value, String groupId) {
        return createNote(name, description, value, null, NoteModel.VALUE_SIMPLE_TYPE, groupId);
    }

    public NoteModel createTimeshiftNote(String name, String description, String timeshift,  String groupId) {
        return createNote(name, description, null, timeshift, NoteModel.TIMESHIFT_SIMPLE_TYPE, groupId);
    }

    private NoteModel createNote(String name, String description, String value, String timeshift, int type, String groupId) {
        if (!isUserLoggedIn()) return null;
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference newNoteReference = FirebaseDatabase.getInstance().getReference()
                .child(USERS)
                .child(uid)
                .child(NOTES).push();
        String key = newNoteReference.getKey();
        String now = new DateTime().toString();
        NoteModel note = new NoteModel(key,name,description,value,now,now,timeshift,type,groupId);
        Map<String,Object> noteMap = note.toMap();
        newNoteReference.updateChildren(noteMap)
            .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
        return note;
    }

    public interface NotesChangedListener{
        void noteAdded(NoteModel note,String previous);
        void noteChanged(NoteModel note,String previous);
        void noteMoved(NoteModel note,String previous);
        void noteRemoved(NoteModel note);
    }

    public void watchNotes(Object who, final NotesChangedListener listener) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(USERS).child(uid);
        Query query = reference.child(NOTES).orderByChild(NoteModel.UPDATED);
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChild) {
                NoteModel noteModel = dataSnapshot.getValue(NoteModel.class);
                noteModel.setId(dataSnapshot.getKey());
                listener.noteAdded(noteModel,previousChild);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChild) {
                NoteModel noteModel = dataSnapshot.getValue(NoteModel.class);
                noteModel.setId(dataSnapshot.getKey());
                listener.noteChanged(noteModel,previousChild);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                NoteModel noteModel = dataSnapshot.getValue(NoteModel.class);
                noteModel.setId(dataSnapshot.getKey());
                listener.noteRemoved(noteModel);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChild) {
                NoteModel noteModel = dataSnapshot.getValue(NoteModel.class);
                noteModel.setId(dataSnapshot.getKey());
                listener.noteMoved(noteModel,previousChild);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //TODO:
            }
        };
        memorizeListener(who,childEventListener,query);
        query.addChildEventListener(childEventListener);
    }

    private void memorizeListener(Object who, ChildEventListener listener, Query query) {
        memorizedChildEventListener.add(new MemorizedChildEventListener(who,listener,query));
    }

    public void detachListeners(Object who) {
        for (int i=0;i<memorizedChildEventListener.size();i++) {
            MemorizedChildEventListener memo = memorizedChildEventListener.get(i);
            if (memo!=null && memo.whoKey==who) {
                memorizedChildEventListener.remove(i);
                i--;
                if (memo.getQuery()!=null) {
                    memo.getQuery().removeEventListener(memo.getListener());
                }
            }
        }
    }

    public interface NoteLoadedListener {
        void noteLoaded(NoteModel note);
        void groupLoaded(GroupModel group);
    }

    public void loadNote(String key, final NoteLoadedListener listener) {
        if (!isUserLoggedIn()) return;
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase.getInstance().getReference()
            .child(USERS)
            .child(uid)
            .child(NOTES)
            .child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    NoteModel note = dataSnapshot.getValue(NoteModel.class);
                    note.setId(dataSnapshot.getKey());
                    listener.noteLoaded(note);
                    loadGroupById(note.getGroupId(), new GroupLoadedListener() {
                        @Override
                        public void groupLoaded(GroupModel group) {
                            listener.groupLoaded(group);
                        }
                    });
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
    }

    public void saveNoteName(String id, String name) {
        //TODO: неплохо бы проверить, что я обновляю имя у существующей заметки
        if (!isUserLoggedIn()) return;
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase.getInstance().getReference()
                .child(USERS)
                .child(uid)
                .child(NOTES)
                .child(id)
                .child(NoteModel.NAME).setValue(name)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //TODO:
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //TODO:
                    }
                });
    }

    public void saveNoteGroupId(String noteId, String groupKey) {
        if (!isUserLoggedIn()) return;
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase.getInstance().getReference()
                .child(USERS)
                .child(uid)
                .child(NOTES)
                .child(noteId)
                .child(NoteModel.GROUP_ID).setValue(groupKey)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //TODO:
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //TODO:
                    }
                });
    }

    public void saveNote(NoteModel note) {
        if (!isUserLoggedIn()) return;
        String now = new DateTime().toString();
        note.setUpdated(now);
        HistoryModel history = new HistoryModel(note.getUpdated(),
                note.getType(),
                note.getValue(),
                note.getTimeshift(),
                note.getDescription());
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Map<String,Object> noteMap = note.toMap();
        FirebaseDatabase.getInstance().getReference()
                .child(USERS)
                .child(uid)
                .child(NOTES)
                .child(note.getId())
                .updateChildren(noteMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //TODO:
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //TODO:
                    }
                });
        FirebaseDatabase.getInstance().getReference()
                .child(USERS)
                .child(uid)
                .child(HISTORY)
                .child(note.getId())
                .push()
                .setValue(history)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //TODO:
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //TODO:
                    }
                });
    }

    private void archiveNote(NoteModel note) {
        if (!isUserLoggedIn()) return;
        if (note==null || note.getId()==null) return;
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child(USERS)
                .child(uid);
        reference.child(ARCHIVE).child(note.getId()).updateChildren(note.toMap());
        reference.child(NOTES).child(note.getId()).removeValue();
    }

    public void archiveNote(String key) {
        loadNote(key, new NoteLoadedListener() {
            @Override
            public void noteLoaded(NoteModel note) {
                archiveNote(note);
            }

            @Override
            public void groupLoaded(GroupModel group) {}
        });
    }

    public interface GroupsLoadedListener {
        void groupsLoaded(List<GroupModel> groups);
    }

    public void loadGroups(final GroupsLoadedListener listener) {
        if (!isUserLoggedIn()) return;
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(USERS).child(uid);
        Query query = reference.child(GROUPS).orderByChild(GroupModel.NAME);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<GroupModel> groups = new ArrayList<>();
                for (DataSnapshot groupSnapshot: dataSnapshot.getChildren()) {
                    GroupModel groupModel = groupSnapshot.getValue(GroupModel.class);
                    groupModel.setKey(groupSnapshot.getKey());
                    groups.add(groupModel);
                }
                listener.groupsLoaded(groups);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Getting Post failed, log a message
                //Log.w(TAG, "loadGroups:onCancelled", databaseError.toException());
                // ...

            }
        });
    }

    public interface GroupLoadedListener {
        void groupLoaded(GroupModel group);
    }

    public void loadGroupById(String groupId, final GroupLoadedListener listener) {
        if (!isUserLoggedIn()) return;
        if (groupId==null) {
            listener.groupLoaded(null);
            return;
        }
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase.getInstance().getReference()
                .child(USERS)
                .child(uid)
                .child(GROUPS)
                .child(groupId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    GroupModel groupModel = dataSnapshot.getValue(GroupModel.class);
                    if (groupModel!=null) {
                        groupModel.setKey(dataSnapshot.getKey());
                        listener.groupLoaded(groupModel);
                    } else listener.groupLoaded(null);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Getting Post failed, log a message
                //Log.w(TAG, "loadGroups:onCancelled", databaseError.toException());
                // ...

            }
        });
    }

    public interface GroupsChangedListener {
        void groupAdded(GroupModel group,String previous);
        void groupChanged(GroupModel group,String previous);
        void groupRemoved(GroupModel group);
        void groupMoved(GroupModel group,String previous);
    }

    public void watchGroups(Object who, final GroupsChangedListener listener) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(USERS).child(uid);
        Query query = reference.child(GROUPS).orderByChild(GroupModel.NAME);
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChild) {
                GroupModel group = dataSnapshot.getValue(GroupModel.class);
                group.setKey(dataSnapshot.getKey());
                listener.groupAdded(group,previousChild);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChild) {
                GroupModel group = dataSnapshot.getValue(GroupModel.class);
                group.setKey(dataSnapshot.getKey());
                listener.groupChanged(group,previousChild);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                GroupModel group = dataSnapshot.getValue(GroupModel.class);
                group.setKey(dataSnapshot.getKey());
                listener.groupRemoved(group);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChild) {
                GroupModel group = dataSnapshot.getValue(GroupModel.class);
                group.setKey(dataSnapshot.getKey());
                listener.groupMoved(group,previousChild);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //TODO:
            }
        };
        memorizeListener(who,childEventListener,query);
        query.addChildEventListener(childEventListener);
    }

    public GroupModel createNewGroup(String name) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(USERS).child(uid);
        DatabaseReference newGroupReference = reference.child(GROUPS).push();
        String key = newGroupReference.getKey();
        GroupModel group = new GroupModel(key,name);
        newGroupReference.child(GroupModel.NAME).setValue(name)
            .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    //TODO:
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //TODO:
                }
            });
        return group;
    }

    public void renameGroup(String key, String newGroupName) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(USERS).child(uid);
        reference.child(GROUPS).child(key).child(GroupModel.NAME).setValue(newGroupName)
            .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    //TODO:
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //TODO:
                }
            });
    }


    public void removeGroup(final String key) {
        if (key==null) return;
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(USERS).child(uid);
        reference.child(GROUPS).child(key).removeValue();
        //TODO: read EVERY notes, need to optimise?
        reference.child(NOTES).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> noteIdsToUpdate = new ArrayList<>();
                for (DataSnapshot noteSnapshot: dataSnapshot.getChildren()) {
                    String noteKey = noteSnapshot.getKey();
                    String noteGroupId = (String)noteSnapshot.child(NoteModel.GROUP_ID).getValue();
                    if (key.equals(noteGroupId)) noteIdsToUpdate.add(noteKey);
                }
                if (noteIdsToUpdate.size()>0) clearGroupInNotes(noteIdsToUpdate);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void clearGroupInNotes(List<String> noteKeys) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(USERS).child(uid).child(NOTES);
        String emptyGroupKey = "";
        for (String key : noteKeys) {
            reference.child(key).child(NoteModel.GROUP_ID).setValue(emptyGroupKey);
        }
    }

    public interface HistoryChangedListener{
        void historyAdded(HistoryModel history,String previous);
        void historyChanged(HistoryModel history,String previous);
        void historyMoved(HistoryModel history,String previous);
        void historyRemoved(HistoryModel history);
    }

    public void watchHistory(Object who, final String noteId, final HistoryChangedListener listener) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(USERS).child(uid);
        Query query = reference.child(HISTORY).child(noteId).orderByChild(HistoryModel.UPDATED);
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChild) {
                HistoryModel history = dataSnapshot.getValue(HistoryModel.class);
                history.setId(dataSnapshot.getKey());
                history.setNoteId(noteId);
                listener.historyAdded(history,previousChild);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChild) {
                HistoryModel history = dataSnapshot.getValue(HistoryModel.class);
                history.setId(dataSnapshot.getKey());
                history.setNoteId(noteId);
                listener.historyChanged(history,previousChild);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                HistoryModel history = dataSnapshot.getValue(HistoryModel.class);
                history.setId(dataSnapshot.getKey());
                history.setNoteId(noteId);
                listener.historyRemoved(history);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChild) {
                HistoryModel history = dataSnapshot.getValue(HistoryModel.class);
                history.setId(dataSnapshot.getKey());
                history.setNoteId(noteId);
                listener.historyMoved(history,previousChild);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //TODO:
            }
        };
        memorizeListener(who,childEventListener,query);
        query.addChildEventListener(childEventListener);
    }


}
