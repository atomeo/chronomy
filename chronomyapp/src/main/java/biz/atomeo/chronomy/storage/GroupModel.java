package biz.atomeo.chronomy.storage;

/**
 * Created by pavelstakhov on 14.06.18.
 */

public class GroupModel {

    public static final String NAME = "name";

    private transient String key;
    private String name;

    public GroupModel() {
    }

    public GroupModel(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setName(String name) {
        this.name = name;
    }
}
