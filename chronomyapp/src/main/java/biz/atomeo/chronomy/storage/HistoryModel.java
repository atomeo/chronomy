package biz.atomeo.chronomy.storage;

/**
 * Created by pavelstakhov on 14.06.18.
 */

public class HistoryModel {

    public static final String UPDATED = "updated";
    public static final String VALUE = "value";
    public static final String TYPE = "type";
    public static final String TIMESHIFT = "timeshift";
    public static final String DESCRIPTION = "description";


    private transient String id;
    private transient String noteId;
    private String updated;
    private int type;
    private String value;
    private String timeshift; //кроме случаев когда событие в будущем, совпадает по дате-времени с updated
    private String description;

    public HistoryModel() {
    }

    public HistoryModel(String updated, int type, String value, String timeshift, String description) {
        this.updated = updated;
        this.type = type;
        this.value = value;
        this.timeshift = timeshift;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getNoteId() {
        return noteId;
    }

    public String getUpdated() {
        return updated;
    }

    public int getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public String getTimeshift() {
        return timeshift;
    }

    public String getDescription() {
        return description;
    }
}
