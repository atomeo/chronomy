package biz.atomeo.chronomy.storage;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pavelstakhov on 14.06.18.
 */

@IgnoreExtraProperties
public class NoteModel {

    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String VALUE = "value";
    public static final String TIMESHIFT = "timeshift";
    public static final String CREATED = "created";
    public static final String UPDATED = "updated";
    public static final String TYPE = "type";
    public static final String GROUP_ID = "groupId";

    private transient String id;
    private String name;
    private String description;
    private String value;
    private String created; //дата-время создания
    private String updated; //дата-время последней правки
    private String timeshift; //кроме случаев когда событие в будущем, совпадает по дате-времени с updated
    private int type;
    private String groupId;
    private transient String groupName;

    public static final int TIMESHIFT_SIMPLE_TYPE =1;
    public static final int TIMESHIFT_TYPE = TIMESHIFT_SIMPLE_TYPE;
    //reserved =2
    //reserved =4
    //reserved =8
    //reserved =16
    //reserved =32
    public static final int VALUE_SIMPLE_TYPE =64;
    //reserved =128
    //reserved =256
    //reserved =512
    //reserved =1024
    //reserved =2048
    //reserved =4096
    //reserved =8192
    public static final int VALUE_TYPE = VALUE_SIMPLE_TYPE;

    public NoteModel() {
    }

    public NoteModel(String id, String name, String description, String value, String created, String updated, String timeshift, int type, String groupId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.value = value;
        this.created = created;
        this.updated = updated;
        this.timeshift = timeshift;
        this.type = type;
        this.groupId = groupId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getValue() {
        return value;
    }

    public String getCreated() {
        return created;
    }

    public String getUpdated() {
        return updated;
    }

    public String getTimeshift() {
        return timeshift;
    }

    public int getType() {
        return type;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public void setTimeshift(String timeshift) {
        this.timeshift = timeshift;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(NAME, name);
        result.put(DESCRIPTION, description);
        result.put(VALUE, value);
        result.put(TIMESHIFT,timeshift);
        result.put(CREATED, created);
        result.put(UPDATED, updated);
        result.put(TYPE, type);
        result.put(GROUP_ID, groupId);
        return result;
    }
}
