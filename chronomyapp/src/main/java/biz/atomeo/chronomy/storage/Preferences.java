package biz.atomeo.chronomy.storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

import biz.atomeo.chronomy.ChronomyApp;

public class Preferences {
    private static final String PREFERENCES_FILE = "chronomyprefs";
    private static final String PREF_APP_UUID = "uuid";
    private static final String PREF_LAST_SELECTED_GROUP = "groupId";
    private static final String PREF_LAST_PREMIUM_STATUS = "ad";

    public static String getUUID() {
        SharedPreferences sharedPreferences = ChronomyApp.getApplication().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        if (sharedPreferences==null) throw new RuntimeException();
        return sharedPreferences.getString(PREF_APP_UUID,null);
    }

    public static void saveUUID(String uuid) {
        SharedPreferences sharedPreferences = ChronomyApp.getApplication().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_APP_UUID,uuid);
        editor.apply();
    }

    public static String restoreLastSelectedGroup() {
        SharedPreferences sharedPreferences = ChronomyApp.getApplication().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        if (sharedPreferences==null) return "";
        return sharedPreferences.getString(PREF_LAST_SELECTED_GROUP,"");
    }

    public static void saveSelectedGroup(String groupId) {
        SharedPreferences sharedPreferences = ChronomyApp.getApplication().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_LAST_SELECTED_GROUP,groupId);
        editor.apply();
    }

    public static boolean restorePremiumStatus() {
        SharedPreferences sharedPreferences = ChronomyApp.getApplication().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        if (sharedPreferences==null) return true;
        return sharedPreferences.getBoolean(PREF_LAST_PREMIUM_STATUS,true);
    }

    public static void savePremiumStatus(boolean premium) {
        SharedPreferences sharedPreferences = ChronomyApp.getApplication().getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_LAST_PREMIUM_STATUS,premium);
        editor.apply();
    }
}
