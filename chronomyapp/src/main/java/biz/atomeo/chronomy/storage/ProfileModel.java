package biz.atomeo.chronomy.storage;

public class ProfileModel {
    private String fullName;
    private String email;
    private boolean premiumAccount;
    private String paidUntil;

    public ProfileModel() {
    }

    public ProfileModel(String fullName, String email, boolean premiumAccount, String paidUntil) {
        this.fullName = fullName;
        this.email = email;
        this.premiumAccount = premiumAccount;
        this.paidUntil=paidUntil;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isPremiumAccount() {
        return premiumAccount;
    }

    public void setPremiumAccount(boolean premiumAccount) {
        this.premiumAccount = premiumAccount;
    }

    public String getPaidUntil() {
        return paidUntil;
    }

    public void setPaidUntil(String paidUntil) {
        this.paidUntil = paidUntil;
    }
}
