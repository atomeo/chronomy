package biz.atomeo.chronomy.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.DatePicker;

import org.joda.time.DateTime;

/**
 * Created by stakhov-pv on 02.02.2016.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    final public static  String DATE_TIME = "datetime";

    public static DatePickerFragment showDatePicker(String dateTime, FragmentManager fragmentManager) {
        DatePickerFragment dialogFragment = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putString(DatePickerFragment.DATE_TIME, dateTime);
        dialogFragment.setArguments(args);
        dialogFragment.show(fragmentManager, "Select Date Dialog");
        return dialogFragment;
    }

    public interface OnDateSetListener {
        void onDateSet(String dateTime);
    }

    OnDateSetListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DateTime dt = truncateDTArgs(this);

        return new DatePickerDialog(getActivity(), this, dt.getYear(), dt.getMonthOfYear()-1, dt.getDayOfMonth());
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

        DateTime dt = truncateDTArgs(this);

        dt=dt.withYear(year);
        dt=dt.withMonthOfYear(month+1);
        dt=dt.withDayOfMonth(day);

        if (listener !=null) listener.onDateSet(dt.toString());
    }

    public static DateTime truncateDTArgs(DialogFragment dlg)  {
        DateTime dt = new DateTime();
        Bundle args = dlg.getArguments();
        if (args!=null) {
            String inputDate = args.getString(DATE_TIME, dt.toString());
            try {
                dt = DateTime.parse(inputDate);
            } catch (Exception e) {
            }
        }
        return dt;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnDateSetListener)context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnDateSetListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener =null;
    }
}
