package biz.atomeo.chronomy.ui;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import org.joda.time.DateTime;


/**
 * Created by stakhov-pv on 12.02.2016.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    public interface OnTimeSetListener {
        void onTimeSet(String dateTime);
    }

    public static TimePickerFragment showTimePicker(String dateTime, FragmentManager fragmentManager) {
        TimePickerFragment dialogFragment = new TimePickerFragment();
        Bundle args = new Bundle();
        args.putString(DATE_TIME,dateTime);
        dialogFragment.setArguments(args);
        dialogFragment.show(fragmentManager, "Select Time Dialog");
        return dialogFragment;
    }

    final public static  String DATE_TIME = "datetime";

    OnTimeSetListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DateTime dt = DatePickerFragment.truncateDTArgs(this);

        return new TimePickerDialog(getActivity(), this, dt.getHourOfDay(), dt.getMinuteOfHour(), DateFormat.is24HourFormat(getActivity()));
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        DateTime dt = DatePickerFragment.truncateDTArgs(this);

        dt=dt.withHourOfDay(hour);
        dt=dt.withMinuteOfHour(minute);

        if (mListener!=null) mListener.onTimeSet(dt.toString());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnTimeSetListener)context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnTimeSetListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener=null;
    }
}
