package biz.atomeo.chronomy.ui.about;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import biz.atomeo.chronomy.ChronomyApp;
import biz.atomeo.chronomy.R;

/**
 * Created by pavelstakhov on 28.05.18.
 */
public class AboutActivity extends AppCompatActivity {

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.bLicenses).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChronomyApp.getApplication(), LicensesActivity.class);
                startActivity(intent);
                finish();
            }
        });

        findViewById(R.id.bPrivacyPolicy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri address = Uri.parse(getString(R.string.privacy_address));
                Intent intent = new Intent(Intent.ACTION_VIEW, address);
                startActivity(intent);
                finish();
            }
        });
        /*
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);
        */
    }

    @Override
    protected void onResume() {
        super.onResume();
        //mAdView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //mAdView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //mAdView.destroy();
    }
}
