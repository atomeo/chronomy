package biz.atomeo.chronomy.ui.about;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import biz.atomeo.chronomy.R;

public class LicensesActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_licenses);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView textView = findViewById(R.id.tvLicenses);

        String licenses = loadLicenses();
        if (licenses.length()==0) {
            Toast.makeText(this,"Error loading licenses info.",Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(licenses,Html.FROM_HTML_MODE_LEGACY));
        } else {
            textView.setText(Html.fromHtml(licenses));
        }
    }

    private String loadLicenses() {
        StringBuffer html = new StringBuffer();
        try {
            InputStream htmlIS = getResources().openRawResource(R.raw.licenses);
            BufferedReader fbr = new BufferedReader(new InputStreamReader(htmlIS));
            while (fbr.ready()) {
                html.append(fbr.readLine());
                html.append(" ");
            }
        } catch (Exception e) {
            //TODO:
            return "";
        }

        return html.toString();
    }

}
