package biz.atomeo.chronomy.ui.groups;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.storage.GroupModel;

/**
 * Created by Администратор on 02.09.2015.
 */
public class GroupCreateDialogFragment extends DialogFragment implements TextView.OnEditorActionListener {

    public interface OnGroupCreatedListener {
        void onGroupCreated(String groupKey, String groupName, String noteId);
    }

    public static GroupCreateDialogFragment show(String noteId, FragmentManager fragmentManager) {
        GroupCreateDialogFragment dialogFragment = new GroupCreateDialogFragment();
        Bundle args = new Bundle();
        args.putString(NOTE_ID_ARG,noteId);
        dialogFragment.setArguments(args);
        dialogFragment.show(fragmentManager,"Create Group Dialog");
        return dialogFragment;
    }

    private static final String NOTE_ID_ARG = "note_id";

    private OnGroupCreatedListener listener;
    private EditText etName;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v=inflater.inflate(R.layout.group_create_dialog, null);
        etName = v.findViewById(R.id.etName);
        etName.setOnEditorActionListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(v)
                .setTitle(R.string.dialog_create_group_header)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        onOk();
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, null );
        return builder.create();
    }

    public void onOk() {
        Bundle args = getArguments();
        if (etName!=null && args!=null) {
            String newCategoryName = etName.getText().toString();
            if (!newCategoryName.isEmpty()) {
                //TODO: убрать отсюда вызов базы
                GroupModel group = FBDatabase.getInstance().createNewGroup(newCategoryName);
                if (listener != null) {
                    listener.onGroupCreated(group.getKey(), group.getName(), args.getString(NOTE_ID_ARG));
                }
            }
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE ||
            actionId == EditorInfo.IME_ACTION_SEND ||
            (event.getKeyCode() == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN)) {
            onOk();
            dismiss();
        }
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnGroupCreatedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnGroupCreatedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

}
