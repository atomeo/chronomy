package biz.atomeo.chronomy.ui.groups;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.GroupModel;

/**
 * Created by Администратор on 08.09.2015.
 */
public class GroupDeleteDialogFragment extends DialogFragment {

    public interface OnGroupDeleteDialogListener {
        void confirmDeleteGroup(String key, String name);
    }

    public static GroupDeleteDialogFragment show(GroupModel group, FragmentManager fragmentManager) {
        if (group==null) throw new IllegalArgumentException();
        GroupDeleteDialogFragment dialogFragment = new GroupDeleteDialogFragment();
        Bundle args = new Bundle();
        args.putString(GROUP_NAME_ARG,group.getName());
        args.putString(GROUP_KEY_ARG,group.getKey());
        dialogFragment.setArguments(args);
        dialogFragment.show(fragmentManager,"Remove Group Dialog");
        return dialogFragment;
    }

    public static final String GROUP_NAME_ARG = "group_name";
    public static final String GROUP_KEY_ARG = "group_key";

    private OnGroupDeleteDialogListener listener;
    private String groupName;
    private String groupKey;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v=inflater.inflate(R.layout.group_delete_dialog, null);

        Bundle args = getArguments();
        groupName = args.getString(GROUP_NAME_ARG,null);
        groupKey = args.getString(GROUP_KEY_ARG,null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(v)
                .setTitle(getResources().getString(R.string.dialog_delete_group_header) + ": "+groupName)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (listener!=null) listener.confirmDeleteGroup(groupKey,groupName);
                    }
        })
                .setNegativeButton(R.string.dialog_cancel, null );
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnGroupDeleteDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnGroupDeleteDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
