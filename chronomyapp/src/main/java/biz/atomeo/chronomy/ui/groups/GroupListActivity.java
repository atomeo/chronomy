package biz.atomeo.chronomy.ui.groups;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.storage.GroupModel;

public class GroupListActivity extends AppCompatActivity
    implements GroupListView,
        GroupCreateDialogFragment.OnGroupCreatedListener,
        GroupDeleteDialogFragment.OnGroupDeleteDialogListener,
        GroupRenameDialogFragment.OnGroupRenameDialogListener {

    private GroupListPresenter presenter;
    private FloatingActionButton fab;
    private RecyclerView rvList;
    private TextView tvEmptyMessage;
    private GroupModel lastSelectedGroupForContextMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //if (savedInstanceState==null) {
            presenter = new GroupListPresenter();
            presenter.setModel(FBDatabase.getInstance());
        //} else {
        //    presenter = PresenterManager.getInstance().restorePresenter(savedInstanceState);
        //}

        setContentView(R.layout.activity_group_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onSelectCreateGroup();
            }
        });

        rvList = findViewById(R.id.rvList);
        registerForContextMenu(rvList);
        rvList.setHasFixedSize(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvList.setLayoutManager(layoutManager);

        rvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0){
                    fab.hide();
                } else{
                    fab.show();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        tvEmptyMessage = findViewById(R.id.tvEmptyMessage);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unbindView();
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.groupcontext, contextMenu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_group_show:
                if (lastSelectedGroupForContextMenu!=null && presenter!=null) {
                    presenter.onSelectShowGroup(lastSelectedGroupForContextMenu);
                }
                return true;
            case R.id.action_group_rename:
                if (lastSelectedGroupForContextMenu!=null && presenter!=null) {
                    presenter.onSelectRenameGroup(lastSelectedGroupForContextMenu);
                }
                return true;
            case R.id.action_group_remove:
                if (lastSelectedGroupForContextMenu!=null && presenter!=null) {
                    presenter.onSelectRemoveGroup(lastSelectedGroupForContextMenu);
                }
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
        presenter=null;
    }

    @Override
    public void close() {
        finish();
    }

    public void setGroups(List<GroupModel> groups) {
        rvList.setAdapter(new GroupListRecyclerViewAdapter(groups));
        showListIfNeed();
    }

    @Override
    public void groupInserted(int position) {
        rvList.getAdapter().notifyItemInserted(position);
        rvList.scrollToPosition(position);
        showListIfNeed();
    }

    @Override
    public void groupRemoved(int position) {
        rvList.getAdapter().notifyItemRemoved(position);
        rvList.scrollToPosition(position);
        showListIfNeed();
    }

    @Override
    public void groupMoved(int newPosition, int oldPosition) {
        rvList.getAdapter().notifyItemMoved(oldPosition,newPosition);
        rvList.scrollToPosition(newPosition);
        showListIfNeed();
    }

    @Override
    public void groupChanged(int position) {
        rvList.getAdapter().notifyItemChanged(position);
        rvList.scrollToPosition(position);
        showListIfNeed();
    }

    @Override
    public void showNewGroupDialog() {
        GroupCreateDialogFragment.show(null,getSupportFragmentManager());
    }

    @Override
    public void showRenameGroupDialog(GroupModel group) {
        GroupRenameDialogFragment.show(group,getSupportFragmentManager());
    }

    @Override
    public void showRemoveGroupDialog(GroupModel group) {
        GroupDeleteDialogFragment.show(group,getSupportFragmentManager());
    }

    @Override
    public void confirmDeleteGroup(String key, String name) {
        presenter.onRemoveGroupConfirmed(key,name);
    }

    @Override
    public void renameGroup(String key, String name) {
        presenter.onRenamedGroup(key,name);
    }

    private void showListIfNeed() {
        if (rvList==null || rvList.getAdapter()==null) return;
        int elementsCount = rvList.getAdapter().getItemCount();
        if (elementsCount>0 && tvEmptyMessage.getVisibility()==View.VISIBLE) {
            hideEmptyMessage();
        } else if ( elementsCount==0 && tvEmptyMessage.getVisibility()==View.INVISIBLE) {
            showEmptyMessage();
        }
    }

    private void showEmptyMessage() {
        rvList.setVisibility(View.INVISIBLE);
        tvEmptyMessage.setVisibility(View.VISIBLE);
    }

    private void hideEmptyMessage() {
        rvList.setVisibility(View.VISIBLE);
        tvEmptyMessage.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onGroupCreated(String groupKey, String groupName, String noteId) {
        //Nothing need to do, on this time
    }

    public class GroupListRecyclerViewAdapter extends RecyclerView.Adapter<GroupListActivity.GroupViewHolder> {

        private List<GroupModel> mValues;

        public GroupListRecyclerViewAdapter(List<GroupModel> mValues) {
            this.mValues = mValues;
        }

        @NonNull
        @Override
        public GroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_item, parent, false);
            return new GroupViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final GroupViewHolder holder, int position) {
            GroupModel group = mValues.get(position);
            holder.mGroup = group;
            holder.mTvName.setText(group.getName());

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.onSelectShowGroup(holder.mGroup);
                }
            });
            holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    holder.mView.showContextMenu();
                    lastSelectedGroupForContextMenu = holder.mGroup;
                    return true;
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }


    }

    public class GroupViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public GroupModel mGroup;
        public final TextView mTvName;

        public GroupViewHolder(View v) {
            super(v);
            mView = v;

            mTvName =v.findViewById(R.id.tvGroupName);
        }
    }

}
