package biz.atomeo.chronomy.ui.groups;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.storage.GroupModel;
import biz.atomeo.chronomy.storage.NoteModel;
import biz.atomeo.chronomy.storage.Preferences;
import biz.atomeo.chronomy.ui.BasePresenter;

public class GroupListPresenter extends BasePresenter<FBDatabase,GroupListView> {
    private List<GroupModel> groups = new ArrayList<>();

    @Override
    protected void updateView() {

    }

    @Override
    public void bindView(@NonNull GroupListView view) {
        super.bindView(view);
        view().setGroups(groups);
    }

    @Override
    public void setModel(FBDatabase model) {
        super.setModel(model);
        attachDBListeners();
    }

    public void attachDBListeners() {
        //TODO: check groupAdded, Changed, Moved Removed in Edge Conditions
        FBDatabase.getInstance().watchGroups(this, new FBDatabase.GroupsChangedListener() {
            @Override
            public void groupAdded(GroupModel group, String previous) {
                int position=findGroupIndex(previous)+1;
                groups.add(position,group);
                if (view()!=null) view().groupInserted(position);
            }

            @Override
            public void groupChanged(GroupModel group, String previous) {
                int position=findGroupIndex(group.getKey());
                if (position<0) return;
                groups.set(position,group);
                if (view()!=null) view().groupChanged(position);
            }

            @Override
            public void groupRemoved(GroupModel group) {
                int position=findGroupIndex(group.getKey());
                if (position<0) return;
                groups.remove(position);
                if (view()!=null) view().groupRemoved(position);
            }

            @Override
            public void groupMoved(GroupModel group, String previous) {
                int oldPosition=findGroupIndex(group.getKey());
                groups.remove(oldPosition);
                int newPosition=findGroupIndex(previous)+1;
                groups.add(newPosition,group);
                if (view()!=null) view().groupMoved(newPosition,oldPosition);
            }

        });
    }

    private void detachDBListeners() {
        FBDatabase.getInstance().detachListeners(this);
    }

    private int findGroupIndex(String key) {
        if (key==null || groups==null) return -1;
        for (int i=0;i<groups.size();i++) {
            if (key.equals(groups.get(i).getKey())) {
                return i;
            }
        }
        return -1;
    }

    public void onSelectCreateGroup() {
        if (view()!=null) view().showNewGroupDialog();
    }

    public void onSelectRenameGroup(GroupModel group) {
        if (view()!=null) view().showRenameGroupDialog(group);
    }

    public void onSelectRemoveGroup(GroupModel group) {
        if (view()!=null) view().showRemoveGroupDialog(group);
    }

    public void onRemoveGroupConfirmed(String key, String name) {
        model.removeGroup(key);
    }

    public void onRenamedGroup(String key, String name) {
        model.renameGroup(key,name);
    }

    public void onSelectShowGroup(GroupModel group) {
        if (group!=null && view()!=null) {
            Preferences.saveSelectedGroup(group.getKey());
            view().close();
        }
    }
}
