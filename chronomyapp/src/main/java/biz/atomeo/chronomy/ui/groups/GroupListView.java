package biz.atomeo.chronomy.ui.groups;

import java.util.List;

import biz.atomeo.chronomy.storage.GroupModel;

public interface GroupListView {
    void setGroups(List<GroupModel> groups);
    void groupInserted(int position);
    void groupRemoved(int position);
    void groupMoved(int newPosition, int oldPosition);
    void groupChanged(int position);
    void showNewGroupDialog();
    void showRenameGroupDialog(GroupModel group);
    void showRemoveGroupDialog(GroupModel group);
    void close();
}
