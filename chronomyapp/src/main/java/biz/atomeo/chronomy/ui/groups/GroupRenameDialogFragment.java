package biz.atomeo.chronomy.ui.groups;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.GroupModel;

public class GroupRenameDialogFragment extends DialogFragment implements TextView.OnEditorActionListener {

    public interface OnGroupRenameDialogListener {
        void renameGroup(String key, String name);
    }

    public static GroupRenameDialogFragment show(GroupModel group, FragmentManager fragmentManager) {
        if (group==null) throw new IllegalArgumentException();
        GroupRenameDialogFragment dialogFragment = new GroupRenameDialogFragment();
        Bundle args = new Bundle();
        args.putString(GROUP_NAME_ARG,group.getName());
        args.putString(GROUP_KEY_ARG,group.getKey());
        dialogFragment.setArguments(args);
        dialogFragment.show(fragmentManager,"Rename Group Dialog");
        return dialogFragment;
    }

    public static final String GROUP_NAME_ARG = "group_name";
    public static final String GROUP_KEY_ARG = "group_key";

    private OnGroupRenameDialogListener listener;
    private String groupName;
    private String groupKey;

    private EditText etName;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v=inflater.inflate(R.layout.group_create_dialog, null);
        etName = v.findViewById(R.id.etName);
        etName.setOnEditorActionListener(this);

        Bundle args = getArguments();
        groupName = args.getString(GROUP_NAME_ARG,null);
        groupKey = args.getString(GROUP_KEY_ARG,null);


        etName.setText(groupName);
        etName.setSelection(groupName.length());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
            .setView(v)
            .setTitle(R.string.dialog_rename_group_header)
            .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    onOk();
                }
            })
            .setNegativeButton(R.string.dialog_cancel, null );
        return builder.create();
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE ||
                actionId == EditorInfo.IME_ACTION_SEND ||
                (event.getKeyCode() == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN)) {
            onOk();
            dismiss();
        }
        return true;
    }

    private void onOk() {
        if (etName!=null) {
            String newGroupName = etName.getText().toString();
            if (!newGroupName.isEmpty()) {
                if (listener != null) listener.renameGroup(groupKey, newGroupName);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnGroupRenameDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnGroupRenameDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
