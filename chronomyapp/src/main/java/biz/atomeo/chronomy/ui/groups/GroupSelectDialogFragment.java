package biz.atomeo.chronomy.ui.groups;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.storage.GroupModel;

/**
 * Created by Администратор on 09.09.2015.
 */
public class GroupSelectDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    public interface OnGroupSelectedListener {
        void onGroupSelected(String groupKey, String groupName, String noteId);
        void onNewGroupSelected(String noteId);
    }

    public static GroupSelectDialogFragment show(String noteId, FragmentManager fragmentManager) {
        GroupSelectDialogFragment dialogFragment = new GroupSelectDialogFragment();
        Bundle args = new Bundle();
        args.putString(NOTE_ID_ARG,noteId);
        dialogFragment.setArguments(args);
        dialogFragment.show(fragmentManager,"Select Group Dialog");
        return dialogFragment;
    }

    private static final String NOTE_ID_ARG = "note_id";

    OnGroupSelectedListener listener =null;

    MyAdapter myAdapter;

    public GroupSelectDialogFragment() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        myAdapter = new MyAdapter(getContext(), new ArrayList<GroupModel>() );

        FBDatabase.getInstance().loadGroups(new FBDatabase.GroupsLoadedListener() {
            @Override
            public void groupsLoaded(List<GroupModel> groups) {
                groups.add(0,new GroupModel("",getString(R.string.dialog_no_group)));
                myAdapter.addAll(groups);
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
            .setTitle(R.string.dialog_select_group_header)
            .setCancelable(true)
            .setAdapter(myAdapter, this)
            .setNeutralButton(R.string.dialog_new_group_button, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //DialogFragment gsdf = new GroupCreateDialogFragment();
                    //TODO: why not getSupportFragmentManager()?
                    //gsdf.show(getFragmentManager(), "Create Group Dialog");
                    Bundle args = getArguments();
                    if (listener!=null && args!=null) {
                        listener.onNewGroupSelected(args.getString(NOTE_ID_ARG,null));
                    }
                }
            })
            .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            })
        ;
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnGroupSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnGroupSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        GroupModel group=myAdapter.getItem(which);
        Bundle args = getArguments();
        if (args!=null) {
            if (group.getKey().length() == 0) group.setName(null);
            if (listener != null)
                listener.onGroupSelected(group.getKey(), group.getName(), args.getString(NOTE_ID_ARG,null));
        }
    }

    private static class MyAdapter extends ArrayAdapter<GroupModel> {
        private final ThemedSpinnerAdapter.Helper mDropDownHelper;

        public MyAdapter(@NonNull Context context, @NonNull List<GroupModel> objects) {
            super(context, android.R.layout.simple_list_item_1, objects);
            mDropDownHelper = new ThemedSpinnerAdapter.Helper(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view;

            if (convertView == null) {
                LayoutInflater inflater = mDropDownHelper.getDropDownViewInflater();
                view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            } else {
                view = convertView;
            }

            TextView textView = view.findViewById(android.R.id.text1);
            textView.setText(getItem(position).getName());

            return view;        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position,convertView,parent);
        }

    }
}
