package biz.atomeo.chronomy.ui.init;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

import biz.atomeo.chronomy.ChronomyApp;
import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.ui.notelist.NoteListActivity;

public class StartActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!FBDatabase.getInstance().isUserLoggedIn())
            showLogin();
        else {
            showNoteList();
        }
    }

    private static final int RC_SIGN_IN = 123;

    public void showLogin() {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                //new AuthUI.IdpConfig.PhoneBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build()
                //new AuthUI.IdpConfig.FacebookBuilder().build(),
                //new AuthUI.IdpConfig.TwitterBuilder().build(),
        );

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setTheme(R.style.AppTheme)
                        .setLogo(R.drawable.logo)
                        .build(),
                RC_SIGN_IN);

    }

    public void successfullyLoggedIn() {
        FBDatabase.getInstance().afterLogin(new FBDatabase.AfterLoginListener() {
            @Override
            public void loginCompleted() {
                showNoteList();
            }

            @Override
            public void loginError() {
                showLogin();
            }
        }, ChronomyApp.getApplication().getUUID());
    }

    public void showNoteList() {
        startActivity(new Intent(this, NoteListActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                //Snackbar.make(fab,"Signed succesfully",Snackbar.LENGTH_LONG).show();
                successfullyLoggedIn();

                // ...
            } else {
                //Snackbar.make(fab,"Sign failed",Snackbar.LENGTH_LONG).show();
                finish();
                //close();

                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }
}
