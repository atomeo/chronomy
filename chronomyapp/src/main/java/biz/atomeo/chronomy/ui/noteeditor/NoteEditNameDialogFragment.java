package biz.atomeo.chronomy.ui.noteeditor;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import biz.atomeo.chronomy.R;

/**
 * Created by Paul Stakhov on 24.05.2018.
 */
public class NoteEditNameDialogFragment extends DialogFragment implements TextView.OnEditorActionListener  {

    public interface OnCounterNameEnteredListener {
        void onNewNoteNameEntered(String name, int newNoteType);
        void onEditNoteNameEntered(String name, String id);
    }

    public static final String ARG_NAME="NOTE_NAME";
    public static final String ARG_DIALOG_TYPE="DIALOG_TYPE";
    public static final int DIALOG_TYPE_NEW_NAME = 1;
    public static final int DIALOG_TYPE_EDIT_NAME = 2;
    public static final String ARG_NEW_NOTE_TYPE="NOTE_TYPE";
    public static final String ARG_EDIT_NOTE_ID="NOTE_ID";


    public static NoteEditNameDialogFragment showEditName(String name, String id, FragmentManager fragmentManager) {
        if (name==null || id==null) throw new IllegalArgumentException();
        NoteEditNameDialogFragment dialogFragment = new NoteEditNameDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_DIALOG_TYPE,DIALOG_TYPE_EDIT_NAME);
        args.putString(ARG_NAME,name);
        args.putString(ARG_EDIT_NOTE_ID,id);
        dialogFragment.setArguments(args);
        dialogFragment.show(fragmentManager,"Rename Note Dialog");
        return dialogFragment;
    }

    public static NoteEditNameDialogFragment showNewName(int type, FragmentManager fragmentManager) {
        NoteEditNameDialogFragment dialogFragment = new NoteEditNameDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_DIALOG_TYPE,DIALOG_TYPE_NEW_NAME);
        args.putInt(ARG_NEW_NOTE_TYPE,type);
        dialogFragment.setArguments(args);
        dialogFragment.show(fragmentManager,"New Note Name Dialog");
        return dialogFragment;
    }

    private OnCounterNameEnteredListener listener;
    private EditText etName;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String name=null;
        //if (savedInstanceState==null) {
            Bundle arg = getArguments();
            if (arg!=null) name=arg.getString(ARG_NAME,"");
        //}

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v=inflater.inflate(R.layout.note_name_dialog, null);
        etName = v.findViewById(R.id.etName);
        etName.setOnEditorActionListener(this);
        if (etName!=null && name!=null) etName.setText(name);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(v)
                .setTitle(R.string.dialog_create_counter_name_header)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        onOk();
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, null );
        return builder.create();
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE ||
                actionId == EditorInfo.IME_ACTION_SEND ||
                (event.getKeyCode() == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN)) {
            onOk();
            dismiss();
        }
        return true;
    }

    private void onOk() {
        if (etName!=null) {
            Bundle args = getArguments();
            if (args==null) return;
            String name = etName.getText().toString();
            if (!name.isEmpty() && listener!=null) {
                switch(args.getInt(ARG_DIALOG_TYPE,0)) {
                    case DIALOG_TYPE_NEW_NAME:
                        int type=args.getInt(ARG_NEW_NOTE_TYPE);
                        listener.onNewNoteNameEntered(name,type);
                        break;
                    case DIALOG_TYPE_EDIT_NAME:
                        String id = args.getString(ARG_EDIT_NOTE_ID,null);
                        listener.onEditNoteNameEntered(name,id);
                        break;
                }
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnCounterNameEnteredListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnCounterNameEnteredListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

}
