package biz.atomeo.chronomy.ui.noteeditor;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.storage.GroupModel;
import biz.atomeo.chronomy.storage.HistoryModel;
import biz.atomeo.chronomy.storage.NoteModel;
import biz.atomeo.chronomy.ui.DatePickerFragment;
import biz.atomeo.chronomy.ui.TimePickerFragment;
import biz.atomeo.chronomy.ui.groups.GroupCreateDialogFragment;
import biz.atomeo.chronomy.ui.groups.GroupSelectDialogFragment;

public class NoteEditorActivity extends AppCompatActivity
        implements NoteEditorView,
            NoteValueEditorFragment.OnFragmentInteractionListener,
            NoteEditNameDialogFragment.OnCounterNameEnteredListener,
            GroupCreateDialogFragment.OnGroupCreatedListener,
            GroupSelectDialogFragment.OnGroupSelectedListener,
            NoteRemoveDialogFragment.OnNoteRemoveDialogListener,
            DatePickerFragment.OnDateSetListener,
            TimePickerFragment.OnTimeSetListener,
            TabLayout.OnTabSelectedListener {

    public final static String ARG_COUNTER_ID = "ARG_COUNTER_ID";

    private NoteEditorPresenter presenter;
    private NoteValueEditorFragment valueEditorFragment;
    private NoteHistoryFragment historyFragment;

    private String mCounterId;
    private int mPage=0;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    private TextView mTVName;
    private TextView mTVGroupName;
    private FloatingActionButton fab;

    public static void openCounterEditor(Context context, String id) {
        Intent intent = new Intent(context, NoteEditorActivity.class);
        intent.putExtra(ARG_COUNTER_ID, id);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //if (savedInstanceState==null) {
        presenter = new NoteEditorPresenter();
        presenter.setModel(FBDatabase.getInstance());

        if (savedInstanceState!=null) {
            presenter.setUnsavedValues(
                    savedInstanceState.getString(NoteEditorPresenter.NOTE_VALUE,null),
                    savedInstanceState.getString(NoteEditorPresenter.NOTE_TIMESHIFT,null),
                    savedInstanceState.getString(NoteEditorPresenter.NOTE_DESCRIPTION,null)
            );
        }

        Intent intent = getIntent();
        mCounterId = intent.getStringExtra(ARG_COUNTER_ID);
        presenter.loadNote(mCounterId);
        //} else {
        //    presenter = PresenterManager.getInstance().restorePresenter(savedInstanceState);
        //}

        if (savedInstanceState != null) {
            mPage =savedInstanceState.getInt(PAGE,0);
        }

        setContentView(R.layout.activity_note_editor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(this);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (valueEditorFragment!=null) valueEditorFragment.saveUpdatedValues();
                presenter.onSelectFABDone();
            }
        });

        mTVName = (TextView)findViewById(R.id.tvName);
        mTVName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onSelectEditNameButton();
            }
        });
        mTVGroupName = (TextView)findViewById(R.id.tvGroupName);
        mTVGroupName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onSelectGroupButton();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_note_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_note_rename:
                presenter.onSelectEditNameButton();
                return true;
            case R.id.action_note_change_group:
                presenter.onSelectGroupButton();
                return true;
            case R.id.action_note_save_changes:
                presenter.onSelectFABDone();
                return true;
            case R.id.action_note_remove:
                presenter.onSelectNoteRemoveButton();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static final String PAGE = "PAGE";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        int page = mViewPager.getCurrentItem();
        PagerAdapter adapter = mViewPager.getAdapter();
        mViewPager.setAdapter(null);
        outState.putInt(PAGE,page);
        Map<String,String> unsavedValues = presenter.getUnsavedValues();
        for (Map.Entry<String,String> e:unsavedValues.entrySet()) {
            outState.putString(e.getKey(),e.getValue());
        }
        super.onSaveInstanceState(outState);
        mViewPager.setAdapter(adapter);
        //PresenterManager.getInstance().savePresenter(presenter, outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
        mViewPager.setCurrentItem(mPage);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unbindView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
        presenter=null;
    }

    public void onResumedNoteHistoryFragment() {
        presenter.onResumedNoteHistoryFragment();
    }

    @Override
    public void setName(String name) {
        mTVName.setText(name);
    }

    @Override
    public void setGroupName(String groupName) {

        if (groupName!=null) {
            mTVGroupName.setText(groupName);
        } else {
            mTVGroupName.setText(R.string.no_group_name);
        }

    }

    @Override
    public void setType(int type) {
        if (valueEditorFragment!=null) {
            valueEditorFragment.setType(type);
        }
    }

    @Override
    public void setValue(String value) {
        if (valueEditorFragment!=null) {
            valueEditorFragment.setValue(value);
        }
    }

    @Override
    public void setTimeshift(String timeshift) {
        if (valueEditorFragment!=null) {
            valueEditorFragment.setTimeshift(timeshift);
        }
    }

    @Override
    public void setDescription(String description) {
        if (valueEditorFragment!=null) {
            valueEditorFragment.setDescription(description);
        }
    }

    @Override
    public void setHistory(List<HistoryModel> historyNotes) {
        if (historyFragment!=null) {
            historyFragment.setHistory(historyNotes);
        }
    }

    @Override
    public void historyNoteInserted(int position) {
        if (historyFragment!=null) {
            historyFragment.historyNoteInserted(position);
        }
    }

    @Override
    public void historyNoteRemoved(int position) {

    }

    @Override
    public void historyNoteMoved(int newPosition, int oldPosition) {

    }

    @Override
    public void historyNoteChanged(int position) {

    }

    @Override
    public void showEditNameDialog(String name, String id) {
        NoteEditNameDialogFragment.showEditName(name,id,getSupportFragmentManager());
    }

    @Override
    public void onEditNoteNameEntered(String name, String id) {
        presenter.setName(id,name);
    }

    @Override
    public void onNewNoteNameEntered(String name, int newNoteType) {
        //UNNEEDED METHOD
    }


    @Override
    public void showSelectGroupDialog(String noteId) {
        GroupSelectDialogFragment.show(noteId,getSupportFragmentManager());
    }

    @Override
    public void onGroupSelected(String groupKey, String groupName, String noteId) {
        presenter.setGroup(groupKey, groupName);
    }

    @Override
    public void onNewGroupSelected(String noteId) {
        GroupCreateDialogFragment.show(noteId,getSupportFragmentManager());

    }

    @Override
    public void onGroupCreated(String groupKey, String groupName, String noteId) {
        presenter.setGroup(groupKey, groupName);
    }

    @Override
    public void showRemoveNoteDialog(NoteModel note) {
        NoteRemoveDialogFragment.show(note,getSupportFragmentManager());
    }


    @Override
    public void showDatePickerDialog(String datetime) {
        DatePickerFragment.showDatePicker(datetime,getSupportFragmentManager());
    }

    @Override
    public void showTimePickerDialog(String datetime) {
        TimePickerFragment.showTimePicker(datetime, getSupportFragmentManager());
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void onDateSet(String dateTime) {
        presenter.onDateSelected(dateTime);
    }

    @Override
    public void onTimeSet(String dateTime) {
        presenter.onTimeSelected(dateTime);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mPage = tab.getPosition();
        if (mPage==0) {
            fab.show();
        }
        else {
            fab.hide();
            hideSoftKeyboard(mViewPager);
        }
    }

    private void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm!=null && imm.isActive()) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onValueChanged(String newValue) {
        presenter.setValue(newValue);
    }

    @Override
    public void onDescriptionChanged(String description) {
        presenter.setDescription(description);
    }

    @Override
    public void onCreatedValueEditorFragmentView() {
        presenter.updateFragmentView();
    }

    @Override
    public void onNowPressed() {
        presenter.onSelectNowButton();
    }

    @Override
    public void onSetTimePressed() {
        presenter.onSelectSetTimeButton();
    }

    @Override
    public void confirmRemoveNote(String key, String name) {
        presenter.onNoteRemoveConfirm(key, name);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final int PAGES_COUNT = 2;

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
                if (position==0) return  NoteValueEditorFragment.newInstance();
                else return NoteHistoryFragment.newInstance();

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Object obj = super.instantiateItem(container, position);
            if (position==0) valueEditorFragment = (NoteValueEditorFragment)obj;
            else historyFragment = (NoteHistoryFragment)obj;
            return obj;
        }

        @Override
        public int getCount() {
            return PAGES_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.note_editor_value_tab);
                case 1:
                    return getString(R.string.note_editor_history_tab);
            }
            return null;
        }

    }
}
