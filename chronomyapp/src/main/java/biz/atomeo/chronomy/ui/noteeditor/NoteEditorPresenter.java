package biz.atomeo.chronomy.ui.noteeditor;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.storage.GroupModel;
import biz.atomeo.chronomy.storage.HistoryModel;
import biz.atomeo.chronomy.storage.NoteModel;
import biz.atomeo.chronomy.ui.BasePresenter;

/**
 * Created by pavelstakhov on 16.05.18.
 */

public class NoteEditorPresenter extends BasePresenter<FBDatabase,NoteEditorView> {
    public final static String NOTE_VALUE = "NOTE_VALUE";
    public final static String NOTE_TIMESHIFT = "NOTE_TIMESHIFT";
    public final static String NOTE_DESCRIPTION = "NOTE_DESCRIPTION";

    private NoteModel note = null; //new NoteModel(); //Empty note for null safety
    private List<HistoryModel> historyModelList = new ArrayList<>();
    private Map<String,String> unsavedValuesMap;

    @Override
    protected void updateView() {
        if (note!=null && view()!=null) {
            view().setName(note.getName());
            view().setGroupName(note.getGroupName());
            updateFragmentView();
        }
    }

    public void updateFragmentView() {
        if (note!=null && view()!=null) {
            view().setDescription(note.getDescription());
            view().setType(note.getType());
            view().setValue(note.getValue());
            view().setTimeshift(biz.atomeo.chronomy.util.DateTime.shortDate(note.getTimeshift()));
        }
    }

    public void onResumedNoteHistoryFragment() {
        if (view()!=null) {
            view().setHistory(historyModelList);
        }
    }

    private void setNote(NoteModel note) {
        this.note=note;
        applyUnsavedValues();
        updateView();
        //updateFragmentView();
    }

    public void loadNote(String noteId) {
        FBDatabase.getInstance().loadNote(noteId, new FBDatabase.NoteLoadedListener() {
            @Override
            public void noteLoaded(NoteModel note) {
                setNote(note);
            }

            @Override
            public void groupLoaded(GroupModel group) {
                if (note!=null && group!=null) {
                    note.setGroupName(group.getName());
                    updateView();
                }
            }

        });

        FBDatabase.getInstance().watchHistory(this, noteId, new FBDatabase.HistoryChangedListener() {
            @Override
            public void historyAdded(HistoryModel history, String previous) {
                //TODO: change
                int pos=0;
                historyModelList.add(pos,history);
                if (view()!=null) view().historyNoteInserted(pos);
            }

            @Override
            public void historyChanged(HistoryModel history, String previous) {

            }

            @Override
            public void historyMoved(HistoryModel history, String previous) {

            }

            @Override
            public void historyRemoved(HistoryModel history) {

            }
        });
    }

    public void setGroup(String groupKey, String groupName) {
        note.setGroupId(groupKey);
        note.setGroupName(groupName);
        FBDatabase.getInstance().saveNoteGroupId(note.getId(),note.getGroupId());
        updateView();
    }

    public void setName(String id,String name) {
        note.setName(name);
        FBDatabase.getInstance().saveNoteName(id,name);
        updateView();
    }

    public void setValue(String value) {
        note.setValue(value);
    }

    public void setTimeshift(String timeshift) {
        note.setTimeshift(timeshift);
    }

    public void setDescription(String description) {
        note.setDescription(description);
        updateView();
    }

    public void setUnsavedValues(String value, String timeshift, String description) {
        unsavedValuesMap = new HashMap<String,String>();
        if (value!=null) unsavedValuesMap.put(NOTE_VALUE,value);
        if (timeshift!=null) unsavedValuesMap.put(NOTE_TIMESHIFT,timeshift);
        if (description!=null) unsavedValuesMap.put(NOTE_DESCRIPTION,description);
    }

    public Map<String,String> getUnsavedValues() {
        unsavedValuesMap = new HashMap<String,String>();
        switch (note.getType()) {
            case NoteModel.VALUE_TYPE:
                unsavedValuesMap.put(NOTE_VALUE,note.getValue());
                break;
            case NoteModel.TIMESHIFT_SIMPLE_TYPE:
                unsavedValuesMap.put(NOTE_TIMESHIFT,note.getTimeshift());
                break;
        }
        unsavedValuesMap.put(NOTE_DESCRIPTION,note.getDescription());
        return unsavedValuesMap;
    }

    private void applyUnsavedValues() {
        if (unsavedValuesMap==null || note==null) return;
        for (Map.Entry<String,String> entry:unsavedValuesMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (key.equals(NOTE_VALUE)) {
                note.setValue(value);
            } else if (key.equals(NOTE_TIMESHIFT)) {
                note.setTimeshift(value);
            } else if (key.equals(NOTE_DESCRIPTION)) {
                note.setDescription(value);
            }
        }
        unsavedValuesMap=null;
    }

    public void onSelectEditNameButton() {
        view().showEditNameDialog(note.getName(),note.getId());
    }

    public void onSelectGroupButton() {
        if (note!=null)
            view().showSelectGroupDialog(note.getId());
    }

    public void onSelectNoteRemoveButton() {
        view().showRemoveNoteDialog(note);
    }

    public void onNoteRemoveConfirm(String key, String name) {
        if (key!=null && note!=null && key.equals(note.getId())) {
            FBDatabase.getInstance().archiveNote(key);
            view().close();
        }
    }

    public void onDateSelected(String dateTime) {
        view().showTimePickerDialog(dateTime);
    }

    public void onTimeSelected(String dateTime) {
        setTimeshift(dateTime);
        view().setTimeshift(biz.atomeo.chronomy.util.DateTime.shortDate(note.getTimeshift()));
    }

    public void onSelectNowButton() {
        onTimeSelected(new DateTime().toString());
    }

    public void onSelectSetTimeButton() {
        view().showDatePickerDialog(note.getTimeshift());
    }

    public void onSelectFABDone() {
        FBDatabase.getInstance().saveNote(note);
        //TODO: model.saveNewStateToDBAndHistory();
        view().close();
    }

    @Override
    public void destroy() {
        super.destroy();
        FBDatabase.getInstance().detachListeners(this);
    }
}
