package biz.atomeo.chronomy.ui.noteeditor;

import java.util.List;

import biz.atomeo.chronomy.storage.HistoryModel;
import biz.atomeo.chronomy.storage.NoteModel;

/**
 * Created by pavelstakhov on 16.05.18.
 */

interface NoteEditorView {
    void setName(String name);
    void setGroupName(String groupName);
    void setType(int type);
    void setValue(String value);
    void setTimeshift(String timeshift);
    void setDescription(String description);
    void setHistory(List<HistoryModel> historyNotes);
    void historyNoteInserted(int position);
    void historyNoteRemoved(int position);
    void historyNoteMoved(int newPosition, int oldPosition);
    void historyNoteChanged(int position);
    void showEditNameDialog(String name,String id);
    void showSelectGroupDialog(String noteId);
    void showRemoveNoteDialog(NoteModel note);
    void showDatePickerDialog(String datetime);
    void showTimePickerDialog(String datetime);
    void close();
}
