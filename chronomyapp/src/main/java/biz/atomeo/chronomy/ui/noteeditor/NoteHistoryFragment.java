package biz.atomeo.chronomy.ui.noteeditor;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.HistoryModel;
import biz.atomeo.chronomy.storage.NoteModel;
import biz.atomeo.chronomy.util.DateTime;

/**
 * Created by pavelstakhov on 16.05.18.
 */

public class NoteHistoryFragment extends Fragment {
    private RecyclerView rvHistory;

    public static NoteHistoryFragment newInstance() {
        NoteHistoryFragment fragment = new NoteHistoryFragment();
        return fragment;
    }

    public NoteHistoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_note_history, container, false);
        rvHistory = v.findViewById(R.id.rvHistory);
        rvHistory.setHasFixedSize(false);
        rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        //rvHistory.setAdapter(new HistoryListRecyclerViewAdapter(new ArrayList<HistoryModel>()));

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        NoteEditorActivity activity = (NoteEditorActivity) getActivity();
        if (activity!=null) activity.onResumedNoteHistoryFragment();
    }

    public void setHistory(List<HistoryModel> historyNotes) {
        if (rvHistory!=null) rvHistory.setAdapter(new HistoryListRecyclerViewAdapter(historyNotes));
    }

    public void historyNoteInserted(int pos) {
        if (rvHistory!=null && rvHistory.getAdapter()!=null) {
            rvHistory.getAdapter().notifyItemInserted(pos);
        }
    }




    public class HistoryListRecyclerViewAdapter extends RecyclerView.Adapter<NoteHistoryFragment.HistoryViewHolder> {

        private List<HistoryModel> mValues;

        public HistoryListRecyclerViewAdapter(List<HistoryModel> mValues) {
            this.mValues = mValues;
        }

        @NonNull
        @Override
        public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.note_item_detailed, parent, false);
            return new HistoryViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final HistoryViewHolder holder, int position) {
            HistoryModel history = mValues.get(position);
            holder.mHistory = history;

            String comment = history.getDescription();
            if (comment==null || comment.isEmpty()) {
                comment="";
                holder.mTvComment.setVisibility(View.GONE);
            } else {
                holder.mTvComment.setVisibility(View.VISIBLE);
            }
            holder.mTvComment.setText(comment);

            switch (history.getType()) {
                case NoteModel.TIMESHIFT_SIMPLE_TYPE:
                    //holder.mTvTimeShift.setText(DateTime.dateShift(history.getTimeshift()));
                    holder.mTvTimeShift.setText(DateTime.shortDate(history.getTimeshift()));
                    holder.mTvTimeShift.setVisibility(View.VISIBLE);
                    holder.mTvValue.setVisibility(View.GONE);
                    break;
                case NoteModel.VALUE_SIMPLE_TYPE:
                    String value = history.getValue();
                    if (value==null || value.isEmpty()) value = "Empty";
                    holder.mTvValue.setText(value);
                    holder.mTvTimeShift.setVisibility(View.GONE);
                    holder.mTvValue.setVisibility(View.VISIBLE);
                    break;
                default:
                    holder.mTvTimeShift.setVisibility(View.GONE);
                    holder.mTvValue.setVisibility(View.GONE);
                    break;
            }

            //holder.mTvUpdated.setText(DateTime.dateShift(history.getUpdated()));
            holder.mTvUpdated.setText(DateTime.shortDate(history.getUpdated()));

        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public HistoryModel mHistory;
        public final TextView mTvName;
        public final TextView mTvValue;
        public final TextView mTvTimeShift;
        public final TextView mTvUpdated;
        public final TextView mTvComment;

        public HistoryViewHolder(View v) {
            super(v);
            mView = v;
            mTvName = v.findViewById(R.id.tvName);
            mTvName.setVisibility(View.GONE);
            mTvValue =v.findViewById(R.id.tvValue);
            mTvTimeShift =v.findViewById(R.id.tvTimeShift);
            mTvUpdated =v.findViewById(R.id.tvUpdated);
            mTvComment =v.findViewById(R.id.tvComment);
        }
    }

}
