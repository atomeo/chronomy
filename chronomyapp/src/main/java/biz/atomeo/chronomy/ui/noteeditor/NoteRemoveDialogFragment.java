package biz.atomeo.chronomy.ui.noteeditor;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.NoteModel;

public class NoteRemoveDialogFragment extends DialogFragment {

    public interface OnNoteRemoveDialogListener {
        void confirmRemoveNote(String key, String name);
    }

    public static NoteRemoveDialogFragment show(NoteModel note, FragmentManager fragmentManager) {
        if (note==null) throw new IllegalArgumentException();
        NoteRemoveDialogFragment dialogFragment = new NoteRemoveDialogFragment();
        Bundle args = new Bundle();
        args.putString(NOTE_NAME_ARG,note.getName());
        args.putString(NOTE_KEY_ARG,note.getId());
        dialogFragment.setArguments(args);
        dialogFragment.show(fragmentManager,"Remove Note Dialog");
        return dialogFragment;
    }

    public static final String NOTE_NAME_ARG = "note_name";
    public static final String NOTE_KEY_ARG = "note_key";

    private OnNoteRemoveDialogListener listener;
    private String noteName;
    private String noteKey;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v=inflater.inflate(R.layout.note_delete_dialog, null);

        Bundle args = getArguments();
        noteName = args.getString(NOTE_NAME_ARG,null);
        noteKey = args.getString(NOTE_KEY_ARG,null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(v)
                .setTitle(getResources().getString(R.string.dialog_remove_note_header) + ": "+noteName)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (listener!=null) listener.confirmRemoveNote(noteKey,noteName);
                    }
        })
                .setNegativeButton(R.string.dialog_cancel, null );
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnNoteRemoveDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnNoteRemoveDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
