package biz.atomeo.chronomy.ui.noteeditor;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.NoteModel;

/**
 * Created by pavelstakhov on 16.05.18.
 */

public class NoteValueEditorFragment extends Fragment
        implements View.OnClickListener {

    public interface OnFragmentInteractionListener {
        void onValueChanged(String newValue);
        void onDescriptionChanged(String description);
        void onNowPressed();
        void onSetTimePressed();
        void onCreatedValueEditorFragmentView();
    }

    private OnFragmentInteractionListener mListener;

    private EditText mETValue;
    private EditText mETDescription;
    private Button mBSetTime;
    private View mRLValue;
    private View mRLTimeshift;

    public static NoteValueEditorFragment newInstance() {
        NoteValueEditorFragment fragment = new NoteValueEditorFragment();
        return fragment;
    }

    public NoteValueEditorFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_note_value_editor, container, false);

        v.findViewById((R.id.bNow)).setOnClickListener(this);
        mBSetTime = v.findViewById(R.id.bSetTime);
        mBSetTime.setOnClickListener(this);

        mETValue = v.findViewById(R.id.etValue);
        mETValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) saveUpdatedValues();
            }
        });
        //hack to make single line edittext with wordwrap
        mETValue.setSingleLine(true);
        mETValue.setHorizontallyScrolling(false);
        mETValue.setMaxLines(10);

        mETDescription = v.findViewById(R.id.etDescription);
        mETDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) saveUpdatedValues();
            }
        });
        mETDescription.setSingleLine(true);
        mETDescription.setHorizontallyScrolling(false);
        mETDescription.setMaxLines(10);

        mRLValue = v.findViewById(R.id.rlValue);
        mRLTimeshift = v.findViewById(R.id.rlTimeshift);

        setHasOptionsMenu(true);

        if (mListener!=null) mListener.onCreatedValueEditorFragmentView();
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bNow:
                if (mListener!=null) mListener.onNowPressed();
                break;
            case R.id.bSetTime:
                if (mListener!=null) mListener.onSetTimePressed();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_note_editor, menu);
    }

    public void setType(int type) {
        boolean timeshift = (type & NoteModel.TIMESHIFT_TYPE) != 0;
        boolean value = (type & NoteModel.VALUE_TYPE) != 0;

        if (mRLValue!=null && mRLTimeshift!=null) {
            mRLValue.setVisibility(value ? View.VISIBLE : View.GONE);
            mRLTimeshift.setVisibility(timeshift ? View.VISIBLE : View.GONE);
            //if (mETValue!=null && value) mETValue.requestFocus();

        }
    }

    public void saveUpdatedValues() {
        if (mListener!=null) {
            mListener.onValueChanged(mETValue.getText().toString());
            mListener.onDescriptionChanged(mETDescription.getText().toString());
        }
    }

    public void setValue(String value) {
        if (mETValue!=null) {
            mETValue.setText(value);
        }
    }

    public void setTimeshift(String timeshift) {
        if (mBSetTime!=null) {
            mBSetTime.setText(timeshift);
        }
    }

    public void setDescription(String description) {
        if (mETDescription!=null) {
            mETDescription.setText(description);
        }
    }

}
