package biz.atomeo.chronomy.ui.notelist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.content.Context;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.content.res.Resources.Theme;

import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import biz.atomeo.chronomy.ChronomyApp;
import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.storage.GroupModel;
import biz.atomeo.chronomy.storage.NoteModel;
import biz.atomeo.chronomy.storage.Preferences;
import biz.atomeo.chronomy.ui.about.AboutActivity;
import biz.atomeo.chronomy.ui.groups.GroupCreateDialogFragment;
import biz.atomeo.chronomy.ui.groups.GroupListActivity;
import biz.atomeo.chronomy.ui.groups.GroupSelectDialogFragment;
import biz.atomeo.chronomy.ui.init.StartActivity;
import biz.atomeo.chronomy.ui.noteeditor.NoteEditNameDialogFragment;
import biz.atomeo.chronomy.ui.noteeditor.NoteEditorActivity;
import biz.atomeo.chronomy.ui.noteeditor.NoteRemoveDialogFragment;
import biz.atomeo.chronomy.ui.profile.ProfileActivity;
import biz.atomeo.chronomy.ui.subscription.SubscribeActivity;
import io.github.kobakei.materialfabspeeddial.FabSpeedDial;

public class NoteListActivity extends AppCompatActivity
        implements
        NoteListView,
        NoteEditNameDialogFragment.OnCounterNameEnteredListener,
        NoteRemoveDialogFragment.OnNoteRemoveDialogListener,
        GroupCreateDialogFragment.OnGroupCreatedListener,
        GroupSelectDialogFragment.OnGroupSelectedListener {

    private final static int NOTE_LIST_ELEMENT_WITH_AD=2;

    private final static int PROFILE_REQUEST_CODE = 1;
    public final static int PROFILE_RESULT_LOGOUT_REQUESTED = 2;

    private final static int SUBSCRIBE_REQUEST_CODE = 3;

    private NoteListPresenter presenter;
    private Spinner spinner;
    private RecyclerView rvList;
    private TextView tvEmptyMessage;
    private io.github.kobakei.materialfabspeeddial.FabSpeedDial fab;
    private AdView adViewRL;
    private NoteModel lastSelectedNoteForContextMenu;
    private boolean showingPayDialog = false;

    private int selectedNoteType;

    static {
        //materialfabspeeddial:
        // In Android 4.x, there is an issue about
        // selector of vector drawable XML. If you want to use such XML,
        // according to StackOverflow, you need to add the following snippet
        // in your Activity classes.
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //if (savedInstanceState==null) {
            presenter = new NoteListPresenter();
            presenter.setModel(FBDatabase.getInstance());
        //} else {
        //    //TODO: после свертывания - проблема развертывания приложения через несколько часов
        //    presenter = PresenterManager.getInstance().restorePresenter(savedInstanceState);
        //}

        setContentView(R.layout.activity_note_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        spinner = findViewById(R.id.spinner);

        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                presenter.groupChoosen(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        fab = findViewById(R.id.fab);
        fab.addOnMenuItemClickListener(new FabSpeedDial.OnMenuItemClickListener() {
            @Override
            public void onMenuItemClick(FloatingActionButton miniFab, @Nullable TextView label, int itemId) {
                switch (itemId) {
                    case R.id.addValueNote:
                        onCreateValueNoteSelected();
                        break;
                    case R.id.addTimeNote:
                        onCreateTimeNoteSelected();
                        break;
                }
            }
        });
        fab.addOnStateChangeListener(new FabSpeedDial.OnStateChangeListener() {
            @Override
            public void onStateChange(boolean open) {

            }
        });

        rvList = findViewById(R.id.rvList);
        rvList.setHasFixedSize(false);
        registerForContextMenu(rvList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rvList.setLayoutManager(layoutManager);

        rvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0){
                    fab.hide();
                } else{
                    fab.show();
                }

                super.onScrolled(recyclerView, dx, dy);
            }
        });

        tvEmptyMessage = findViewById(R.id.tvEmptyMessage);

        setupAD(Preferences.restorePremiumStatus());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adViewRL!=null) adViewRL.resume();
        //TODO: после свертывания - проблема развертывания приложения через несколько часов, presenter=null!
        presenter.bindView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (adViewRL!=null) adViewRL.pause();
        presenter.unbindView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adViewRL!=null) adViewRL.destroy();
        presenter.destroy();
        presenter=null;
    }

    @Override
    public void setupAD(boolean premium) {
        if (premium) {
            if (adViewRL!=null) {
                if (adViewRL.getParent()!=null) {
                    ((ViewGroup)adViewRL.getParent()).removeView(adViewRL);
                }
                adViewRL.destroy();
                adViewRL=null;
            }
        } else {
            if (adViewRL==null) {
                adViewRL = new AdView(this);
                adViewRL.setAdSize(AdSize.BANNER);
                adViewRL.setAdUnitId(getString(R.string.note_list_banner_ad_unit_id));
                AdRequest adRequestRL = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();
                adViewRL.loadAd(adRequestRL);
            }
        }
        Preferences.savePremiumStatus(premium);
    }

    public void setFilter(String searchValue, String groupId) {
        try {
            NoteListRecyclerViewAdapter adapter = (NoteListRecyclerViewAdapter)rvList.getAdapter();
            adapter.filterItems(searchValue, groupId);
            if (adapter.getItemCount()>0) {
                rvList.scrollToPosition(adapter.getItemCount()-1);
            }
        } catch (Exception e) {
            //TODO:
        }
    }

    @Override
    public void setGroups(List<GroupModel> groups, int selected) {
        spinner.setAdapter(new GroupSpinnerAdapter(
                this,
                groups));
        spinner.setSelection(selected);
    }

    public void setNotes(List<NoteModel> notes) {
        rvList.setAdapter(new NoteListRecyclerViewAdapter(notes));
    }

    @Override
    public void noteInserted(int position) {
        NoteListRecyclerViewAdapter adapter = (NoteListRecyclerViewAdapter)rvList.getAdapter();
        if (adapter.isFiltered()) adapter.reFilterItems();
        else {
            adapter.notifyItemInserted(position);
            showListIfNeed(adapter.getItemCount());
        }
    }

    @Override
    public void noteRemoved(int position) {
        NoteListRecyclerViewAdapter adapter = (NoteListRecyclerViewAdapter)rvList.getAdapter();
        if (adapter.isFiltered()) adapter.reFilterItems();
        else {
            adapter.notifyItemRemoved(position);
            showListIfNeed(adapter.getItemCount());
        }
    }

    @Override
    public void noteMoved(int newPosition, int oldPosition) {
        NoteListRecyclerViewAdapter adapter = (NoteListRecyclerViewAdapter)rvList.getAdapter();
        if (adapter.isFiltered()) adapter.reFilterItems();
        else adapter.notifyItemMoved(oldPosition,newPosition);
    }

    @Override
    public void noteChanged(int position) {
        NoteListRecyclerViewAdapter adapter = (NoteListRecyclerViewAdapter)rvList.getAdapter();
        if (adapter.isFiltered()) adapter.reFilterItems();
        else adapter.notifyItemChanged(position);
    }

    private void showListIfNeed(int elementsCount) {
        if (elementsCount>0 && tvEmptyMessage.getVisibility()==View.VISIBLE) {
            hideEmptyMessage();
        } else if ( elementsCount==0 && tvEmptyMessage.getVisibility()==View.INVISIBLE) {
            showEmptyMessage();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_note_list, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                presenter.searchEntered(newText);
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_group_list:
                showGroupList();
                return true;
            case R.id.action_about:
                showAbout();
                return true;
            case R.id.action_profile:
                showProfile();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showLogout() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                        //Snackbar.make(fab,"Signed off",Snackbar.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), StartActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

    }

    public void showGroupList() {
        Intent intent = new Intent(this, GroupListActivity.class);
        startActivity(intent);
    }

    public void showAbout() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void showProfile() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivityForResult(intent,PROFILE_REQUEST_CODE);
    }

    @Override
    public void showPayOrLogoffDialog() {
        if (showingPayDialog) return;
        Intent intent = new Intent(this, SubscribeActivity.class);
        intent.putExtra(SubscribeActivity.SUBSCRIBE_ARG_SHOW_PAY_OR_LOGOF,true);
        startActivityForResult(intent,SUBSCRIBE_REQUEST_CODE);
        showingPayDialog = true;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PROFILE_REQUEST_CODE) {
            if (resultCode== PROFILE_RESULT_LOGOUT_REQUESTED) {
                showLogout();
            } else presenter.rereadProfile();
        } else if (requestCode==SUBSCRIBE_REQUEST_CODE) {
            if (resultCode==SubscribeActivity.SUBSCRIBE_RESULT_FREE_OK_PRESSED) {
                showLogout();
            } else if (resultCode==SubscribeActivity.SUBSCRIBE_RESULT_PREMIUM_OK_PRESSED) {
                showingPayDialog = false;
                presenter.rereadProfile();
            } else {
                showingPayDialog = false;
                presenter.rereadProfile();
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.note_context_menu, contextMenu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (lastSelectedNoteForContextMenu==null || presenter==null) return true;
        switch (item.getItemId()) {
            case R.id.action_note_edit:
                presenter.onNoteSelected(lastSelectedNoteForContextMenu);
                return true;
            case R.id.action_note_rename:
                presenter.onNoteRenameSelected(lastSelectedNoteForContextMenu);
                return true;
            case R.id.action_note_change_group:
                presenter.onNoteChangeGroupSelected(lastSelectedNoteForContextMenu);
                return true;
            case R.id.action_note_archive:
                presenter.onNoteArchiveSelected(lastSelectedNoteForContextMenu);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void openNoteEditor(String key) {
        NoteEditorActivity.openCounterEditor(this,key);
    }

    @Override
    public String getDeviceId() {
        return ChronomyApp.getApplication().getUUID();
    }

    @Override
    public void showSelectGroupDialog(String noteId) {
        GroupSelectDialogFragment.show(noteId,getSupportFragmentManager());
    }

    @Override
    public void showRemoveNoteDialog(NoteModel note) {
        NoteRemoveDialogFragment.show(note,getSupportFragmentManager());
    }

    @Override
    public void confirmRemoveNote(String key, String name) {
        presenter.onNoteRemoveConfirm(lastSelectedNoteForContextMenu, key, name);
    }

    public void setNoNotesYetMessage() {
        tvEmptyMessage.setText(R.string.message_note_list_no_notes_yet);
    }

    public void setNoNotesFoundMessage() {
        tvEmptyMessage.setText(R.string.message_note_list_no_notes_found);
    }

    public void setNoNotesInGroupMessage() {
        tvEmptyMessage.setText(R.string.message_note_list_no_notes_in_group);
    }

    private void showEmptyMessage() {
        rvList.setVisibility(View.INVISIBLE);
        tvEmptyMessage.setVisibility(View.VISIBLE);
    }

    private void hideEmptyMessage() {
        rvList.setVisibility(View.VISIBLE);
        tvEmptyMessage.setVisibility(View.INVISIBLE);
    }

    private void onCreateValueNoteSelected() {
        showNewNoteNameDialog(NoteModel.VALUE_SIMPLE_TYPE);
    }

    private void onCreateTimeNoteSelected() {
        showNewNoteNameDialog(NoteModel.TIMESHIFT_SIMPLE_TYPE);
    }

    @Override
    public void showEditNameDialog(String name) {
        NoteEditNameDialogFragment.showEditName(name,lastSelectedNoteForContextMenu.getId(),getSupportFragmentManager());
    }

    private void showNewNoteNameDialog(int type) {
        NoteEditNameDialogFragment.showNewName(type, getSupportFragmentManager());
    }

    @Override
    public void onNewNoteNameEntered(String name, int newNoteType) {
        if (presenter==null) return;
        presenter.createNote(name,newNoteType);
    }

    @Override
    public void onEditNoteNameEntered(String name, String id) {
        if (presenter==null) return;
        presenter.renameNote(id,name);

    }

    @Override
    public void onGroupSelected(String groupKey, String groupName, String noteId) {
        if (presenter!=null) presenter.onNoteGroupChanged(noteId,groupKey);
    }

    @Override
    public void onNewGroupSelected(String noteId) {
        GroupCreateDialogFragment.show(noteId,getSupportFragmentManager());
    }

    @Override
    public void onGroupCreated(String groupKey, String groupName, String noteId) {
        if (presenter!=null) presenter.onNoteGroupCreated(noteId,groupKey);
    }

    private static class GroupSpinnerAdapter extends ArrayAdapter<GroupModel> implements ThemedSpinnerAdapter {
        private final ThemedSpinnerAdapter.Helper mDropDownHelper;

        public GroupSpinnerAdapter(@NonNull Context context, @NonNull List<GroupModel> objects) {
            super(context, android.R.layout.simple_list_item_1, objects);
            mDropDownHelper = new ThemedSpinnerAdapter.Helper(context);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return getDropDownView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                LayoutInflater inflater = mDropDownHelper.getDropDownViewInflater();
                view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            } else {
                view = convertView;
            }

            TextView textView = view.findViewById(android.R.id.text1);
            textView.setText(getItem(position).getName());

            return view;
        }

        @Override
        public Theme getDropDownViewTheme() {
            return mDropDownHelper.getDropDownViewTheme();
        }

        @Override
        public void setDropDownViewTheme(Theme theme) {
            mDropDownHelper.setDropDownViewTheme(theme);
        }
    }

    public class NoteListRecyclerViewAdapter extends RecyclerView.Adapter<NoteListActivity.NoteViewHolder> {

        private List<NoteModel> mValues;
        private List<NoteModel> mFilteredValues;
        private String actualFilter;
        private String groupId;

        public NoteListRecyclerViewAdapter(List<NoteModel> mValues) {
            this.mValues = mValues;
            this.mFilteredValues = mValues;
        }

        public void filterItems(String filter, String groupId) {
            this.actualFilter=filter;
            this.groupId=groupId;

            //TODO: перенести в presenter
            List<NoteModel> newFilteredValues = new ArrayList<>();
            if (filter!=null && filter.length()>0) {
                String filterLoCase = filter.toLowerCase(Locale.getDefault());
                for (NoteModel note:mValues) {
                    StringBuilder b = new StringBuilder();
                    b.append(note.getName()).append(note.getValue()).append(note.getDescription());
                    if (b.toString().toLowerCase(Locale.getDefault()).contains(filterLoCase)) {
                        newFilteredValues.add(note);
                    }
                }
                setNoNotesFoundMessage();
            } else if (groupId!=null) {
                for (NoteModel note:mValues) {
                    if (groupId.equals(note.getGroupId())) {
                        newFilteredValues.add(note);
                    }
                }
                setNoNotesInGroupMessage();
            } else {
                newFilteredValues = mValues;
                setNoNotesYetMessage();
            }
            mFilteredValues = newFilteredValues;
            notifyDataSetChanged();
            showListIfNeed(mFilteredValues.size());
        }

        public void reFilterItems() {
            if (mFilteredValues!=mValues) {
                filterItems(actualFilter, groupId);
            }
        }

        public boolean isFiltered() {
            return mFilteredValues!=mValues;
        }


        @NonNull
        @Override
        public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.note_item_detailed, parent, false);
            return new NoteViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final NoteViewHolder holder, int position) {
            NoteModel note = mFilteredValues.get(position);
            holder.mNote = note;
            holder.mTvName.setText(note.getName());

            String comment = note.getDescription();
            if (comment==null || comment.isEmpty()) {
                comment="";
                holder.mTvComment.setVisibility(View.GONE);
            } else {
                holder.mTvComment.setVisibility(View.VISIBLE);
            }
            holder.mTvComment.setText(comment);

            switch (note.getType()) {
                case NoteModel.TIMESHIFT_SIMPLE_TYPE:
                    holder.mTvTimeShift.setText(biz.atomeo.chronomy.util.DateTime.dateShift(note.getTimeshift()));
                    holder.mTvTimeShift.setVisibility(View.VISIBLE);
                    holder.mTvValue.setVisibility(View.GONE);
                    break;
                case NoteModel.VALUE_SIMPLE_TYPE:
                    String value = note.getValue();
                    if (value==null || value.isEmpty()) value = getString(R.string.empty_note_value);

                    int valueHeightId = R.dimen.notelist_value_onestring_height;
                    if (value.length()>17) {
                        valueHeightId = R.dimen.notelist_value_twostring_height;
                    }
                    ViewGroup.LayoutParams params = holder.mTvValue.getLayoutParams();
                    params.height = getResources().getDimensionPixelSize(valueHeightId);
                    holder.mTvValue.setLayoutParams(params);

                    holder.mTvValue.setText(value);
                    holder.mTvTimeShift.setVisibility(View.GONE);
                    holder.mTvValue.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }

            holder.mTvUpdated.setText(biz.atomeo.chronomy.util.DateTime.dateShift(note.getUpdated()));

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.onNoteSelected(holder.mNote);
                }
            });

            holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    holder.mView.showContextMenu();
                    lastSelectedNoteForContextMenu = holder.mNote;
                    return true;
                }
            });

            holder.mAdLayout.removeAllViewsInLayout();
            if (adViewRL!=null && position==mFilteredValues.size()-NOTE_LIST_ELEMENT_WITH_AD-1) {
                if (adViewRL.getParent()!=null) {
                    ((ViewGroup)adViewRL.getParent()).removeView(adViewRL);
                }
                holder.mAdLayout.addView(adViewRL);
                holder.mAdLayout.setVisibility(View.VISIBLE);
                holder.mIvDivider.setVisibility(View.INVISIBLE);
            } else {
                holder.mAdLayout.setVisibility(View.GONE);
                holder.mIvDivider.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public int getItemCount() {
            return mFilteredValues.size();
        }


    }

    public class NoteViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public NoteModel mNote;
        public final TextView mTvName;
        public final TextView mTvValue;
        public final TextView mTvTimeShift;
        public final TextView mTvUpdated;
        public final TextView mTvComment;
        public final View mIvDivider;
        public final FrameLayout mAdLayout;

        NoteViewHolder(View v) {
            super(v);
            mView = v;

            mTvName =v.findViewById(R.id.tvName);
            mTvValue =v.findViewById(R.id.tvValue);
            mTvTimeShift =v.findViewById(R.id.tvTimeShift);
            mTvUpdated =v.findViewById(R.id.tvUpdated);
            mTvComment =v.findViewById(R.id.tvComment);
            mIvDivider = v.findViewById(R.id.ivDivider);
            mAdLayout = v.findViewById(R.id.adLayout);
        }
    }

}