package biz.atomeo.chronomy.ui.notelist;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import biz.atomeo.chronomy.ChronomyApp;
import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.storage.GroupModel;
import biz.atomeo.chronomy.storage.NoteModel;
import biz.atomeo.chronomy.storage.Preferences;
import biz.atomeo.chronomy.storage.ProfileModel;
import biz.atomeo.chronomy.ui.BasePresenter;

public class NoteListPresenter extends BasePresenter<FBDatabase,NoteListView> {
    private List<NoteModel> notes = new ArrayList<>();
    private List<GroupModel> groups = new ArrayList<>();
    private ProfileModel profile;
    private String selectedGroupId;
    private String loggedInUniqueId;

    @Override
    protected void updateView() {
        updateGroups();
        updatePremium();
    }

    private void updatePremiumStatus() {
        if (loggedInUniqueId==null || view()==null || profile==null) return;
        if (!loggedInUniqueId.equals(view().getDeviceId()) && !profile.isPremiumAccount()) {
            view().showPayOrLogoffDialog();
        }
    }

    private void updatePremium() {
        if (view()!=null && profile!=null) view().setupAD(profile.isPremiumAccount());
    }

    private void updateGroups() {
        if (view()!=null) view().setGroups(groups,restoreSelectedGroupIndex());
    }

    public void onNoteSelected(NoteModel note) {
        if (view()!=null) view().openNoteEditor(note.getId());
    }

    public void onNoteRenameSelected(NoteModel note) {
        if (view()!=null) view().showEditNameDialog(note.getName());
    }

    public void createNote(String name, int type) {
        String groupId = "";
        if (selectedGroupId!=null) groupId=selectedGroupId;
        NoteModel note = null;
        if (type == NoteModel.VALUE_SIMPLE_TYPE) {
            note = FBDatabase.getInstance().createValueNote(name, null, null, groupId);
        } else if (type == NoteModel.TIMESHIFT_SIMPLE_TYPE) {
            note = FBDatabase.getInstance().createTimeshiftNote(name, null, null, groupId);
        }
        if (note != null) {
            view().openNoteEditor(note.getId());
        }
    }

    public void renameNote(String id, String newName) {
        FBDatabase.getInstance().saveNoteName(id,newName);
    }

    public void onNoteArchiveSelected(NoteModel note) {
        if (view()!=null) view().showRemoveNoteDialog(note);
    }

    public void onNoteRemoveConfirm(NoteModel note, String key, String name) {
        FBDatabase.getInstance().archiveNote(key);
    }

    public void onNoteChangeGroupSelected(NoteModel note) {
        if (view()!=null) view().showSelectGroupDialog(note.getId());
    }

    public void onNoteGroupChanged(String noteId, String groupKey) {
        if (noteId!=null) {
            FBDatabase.getInstance().saveNoteGroupId(noteId,groupKey);
        }
    }

    public void onNoteGroupCreated(String noteId, String groupKey) {
        if (noteId!=null) {
            FBDatabase.getInstance().saveNoteGroupId(noteId,groupKey);
            rereadGroups();
        }
    }

    @Override
    public void bindView(@NonNull NoteListView view) {
        super.bindView(view);
        view().setNotes(notes);
        rereadGroups();
        updatePremiumStatus();
    }

    @Override
    public void unbindView() {
        super.unbindView();

    }

    public void rereadGroups() {
        FBDatabase.getInstance().loadGroups(new FBDatabase.GroupsLoadedListener() {
            @Override
            public void groupsLoaded(List<GroupModel> newGroups) {
                groups = newGroups;
                groups.add(0,new GroupModel(null, ChronomyApp.getApplication().getString(R.string.note_list_group_all_notes)));
                updateGroups();
            }
        });
    }

    public void attachDBListeners() {
        rereadGroups();

        //TODO: check noteAdded, Changed, Moved Removed in Edge Conditions
        FBDatabase.getInstance().watchNotes(this, new FBDatabase.NotesChangedListener() {
            @Override
            public void noteAdded(NoteModel note, String previous) {
                int position=findNoteIndex(previous)+1;
                notes.add(position,note);
                if (view()!=null) view().noteInserted(position);
            }

            @Override
            public void noteChanged(NoteModel note, String previous) {
                int position=findNoteIndex(note.getId());
                if (position<0) return;
                notes.set(position,note);
                if (view()!=null) view().noteChanged(position);
            }

            @Override
            public void noteMoved(NoteModel note, String previous) {
                int oldPosition=findNoteIndex(note.getId());
                notes.remove(oldPosition);
                int newPosition=findNoteIndex(previous)+1;
                notes.add(newPosition,note);
                if (view()!=null) view().noteMoved(newPosition,oldPosition);
            }

            @Override
            public void noteRemoved(NoteModel note) {
                int position=findNoteIndex(note.getId());
                if (position<0) return;
                notes.remove(position);
                if (view()!=null) view().noteRemoved(position);
            }
        });

        FBDatabase.getInstance().loadLoggedDevice(new FBDatabase.OnLoadLoggedDevice() {
            @Override
            public void loggedUUIDLoaded(String uuid) {
                loggedInUniqueId = uuid;
                updatePremiumStatus();
            }
        });

        rereadProfile();

    }

    public void detachDBListeners() {
        FBDatabase.getInstance().detachListeners(this);
    }

    public void rereadProfile() {
        FBDatabase.getInstance().loadProfile(new FBDatabase.OnLoadProfile() {
            @Override
            public void profileLoaded(ProfileModel profileModel) {
                profile = profileModel;
                updatePremium();
            }

            @Override
            public void profileNotLoggedIn() {
                //TODO:
            }
        });
    }

    private int findNoteIndex(String key) {
        if (key==null || notes==null) return -1;
        for (int i=0;i<notes.size();i++) {
            if (key.equals(notes.get(i).getId())) {
                return i;
            }
        }
        return -1;
    }

    public int restoreSelectedGroupIndex() {
        String key=Preferences.restoreLastSelectedGroup();
        return findGroupIndex(key);
    }

    private int findGroupIndex(String key) {
        if (key==null || groups==null || key.length()==0) return 0;
        for (int i=0;i<groups.size();i++) {
            if (key.equals(groups.get(i).getKey())) {
                return i;
            }
        }
        return 0;
    }

    public void groupChoosen(int position) {
        if (position>=0 && position<groups.size()) {
            GroupModel group = groups.get(position);
            selectedGroupId = group.getKey();
            if (view()!=null) {
                view().setFilter(null, selectedGroupId);
            }
            Preferences.saveSelectedGroup(selectedGroupId);
        }
    }

    public void searchEntered(String search) {
        if (view()!=null) {
            view().setFilter(search,selectedGroupId);
        }

    }

    @Override
    public void setModel(FBDatabase model) {
        super.setModel(model);
        attachDBListeners();
    }

    @Override
    public void destroy() {
        super.destroy();
        detachDBListeners();
    }
}
