package biz.atomeo.chronomy.ui.notelist;

import java.util.List;

import biz.atomeo.chronomy.storage.GroupModel;
import biz.atomeo.chronomy.storage.NoteModel;

public interface NoteListView {
    //void showLogin();
    void setGroups(List<GroupModel> groups, int selected);
    void setNotes(List<NoteModel> notes);
    void setFilter(String searchValue, String groupId);
    void noteInserted(int position);
    void noteRemoved(int position);
    void noteMoved(int newPosition, int oldPosition);
    void noteChanged(int position);
    void openNoteEditor(String key);
    void showEditNameDialog(String name);
    void showRemoveNoteDialog(NoteModel note);
    void showSelectGroupDialog(String noteId);
    String getDeviceId();
    void showPayOrLogoffDialog();
    void setupAD(boolean premium);
}
