package biz.atomeo.chronomy.ui.profile;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.NoteModel;

public class LogoutDialogFragment extends DialogFragment {

    public interface OnLogoutDialogListener {
        void confirmLogout();
    }

    public static LogoutDialogFragment show(FragmentManager fragmentManager) {
        LogoutDialogFragment dialogFragment = new LogoutDialogFragment();
        dialogFragment.show(fragmentManager,"Logout Dialog");
        return dialogFragment;
    }

    private OnLogoutDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle(R.string.dialog_logout_header)
                .setMessage(R.string.dialog_logout_message)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (listener!=null) listener.confirmLogout();
                    }
        })
                .setNegativeButton(R.string.dialog_cancel, null );
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnLogoutDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnLogoutDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
