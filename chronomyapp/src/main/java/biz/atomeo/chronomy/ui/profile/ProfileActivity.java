package biz.atomeo.chronomy.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.ui.notelist.NoteListActivity;
import biz.atomeo.chronomy.ui.subscription.SubscribeActivity;

public class ProfileActivity extends AppCompatActivity
        implements ProfileView, LogoutDialogFragment.OnLogoutDialogListener {

    ProfilePresenter presenter;
    TextView tvName;
    TextView tvEmail;
    TextView tvAccountType;
    Button bImproveAccount;
    Button bLogout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        presenter = new ProfilePresenter();
        presenter.setModel(FBDatabase.getInstance());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        tvName = findViewById(R.id.tvName);
        tvEmail = findViewById(R.id.tvEmail);
        tvAccountType = findViewById(R.id.tvAccountType);
        bImproveAccount = findViewById(R.id.bImproveAccount);
        bImproveAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onImproveAccountSelected();
            }
        });
        bLogout = findViewById(R.id.bLogout);
        bLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onLogoutSelected();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
        presenter.reloadProfile();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unbindView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
        presenter=null;
    }

    @Override
    public void setFullName(String fullName) {
        tvName.setText(fullName);
    }

    @Override
    public void setEmail(String email) {
        tvEmail.setText(email);
    }

    @Override
    public void setAccountType(String type) {
        tvAccountType.setText(type);
    }

    @Override
    public void showLogoutConfirmDialog() {
        LogoutDialogFragment.show(getSupportFragmentManager());
    }

    @Override
    public void confirmLogout() {
        setResult(NoteListActivity.PROFILE_RESULT_LOGOUT_REQUESTED);
        finish();
    }

    @Override
    public void showSubscription() {
        Intent intent = new Intent(this, SubscribeActivity.class);
        startActivity(intent);
    }
}

