package biz.atomeo.chronomy.ui.profile;

import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.storage.ProfileModel;
import biz.atomeo.chronomy.ui.BasePresenter;

public class ProfilePresenter extends BasePresenter<FBDatabase,ProfileView> implements FBDatabase.OnLoadProfile {
    ProfileModel profile = new ProfileModel("","",false,"");

    @Override
    protected void updateView() {
        if (view()!=null && profile !=null) {
            view().setFullName(profile.getFullName());
            view().setEmail(profile.getEmail());
            if (!("".equals(profile.getFullName()) && "".equals(profile.getEmail()) )) {
                view().setAccountType(profile.isPremiumAccount() ? "Premium" : "Free");
            }
        }
    }

    @Override
    public void setModel(FBDatabase model) {
        super.setModel(model);
        reloadProfile();
    }

    public void reloadProfile() {
        model.loadProfile(this);
    }

    @Override
    public void profileLoaded(ProfileModel profile) {
        if (profile!=null) this.profile=profile;
        updateView();
    }

    @Override
    public void profileNotLoggedIn() {
        //TODO:
    }

    public void onImproveAccountSelected() {
        if (view()!=null) view().showSubscription();
    }

    public void onLogoutSelected() {
        if (view()!=null) view().showLogoutConfirmDialog();
    }
}
