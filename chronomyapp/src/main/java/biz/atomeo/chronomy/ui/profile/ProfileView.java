package biz.atomeo.chronomy.ui.profile;

public interface ProfileView {
    void setFullName(String fullName);
    void setEmail(String email);
    void setAccountType(String type);
    void showLogoutConfirmDialog();
    void showSubscription();
}
