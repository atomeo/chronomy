package biz.atomeo.chronomy.ui.subscription;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;

import java.util.ArrayList;
import java.util.List;

import biz.atomeo.chronomy.R;
import biz.atomeo.chronomy.billing.BillingConstants;
import biz.atomeo.chronomy.billing.BillingManager;
import biz.atomeo.chronomy.storage.FBDatabase;
import biz.atomeo.chronomy.util.DateTime;

public class SubscribeActivity extends AppCompatActivity implements BillingManager.BillingUpdatesListener {

    public static int SUBSCRIBE_RESULT_FREE_OK_PRESSED = 1;
    public static int SUBSCRIBE_RESULT_PREMIUM_OK_PRESSED = 2;
    public static String SUBSCRIBE_ARG_SHOW_PAY_OR_LOGOF="pay_or_logoff";


    private BillingManager mBillingManager;
    private boolean mMonthlySubscription;
    private boolean mYearlySubscription;

    private boolean showPayOrLogoffMessage;
    private boolean showNoSubscribes;

    private Button bBuyMonthly;
    private Button bBuyYearly;
    private Button bOk;
    private TextView tvMessage;
    private TextView tvNoSubscribe;
    private View subscribeButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);

        Bundle args = getIntent().getExtras();
        if (args!=null) {
            showPayOrLogoffMessage = args.getBoolean(SUBSCRIBE_ARG_SHOW_PAY_OR_LOGOF, false);
        } else showPayOrLogoffMessage = false;

        mBillingManager = new BillingManager(this, this);

        tvMessage = findViewById(R.id.tvMessage);
        tvMessage.setVisibility(View.GONE);

        showNoSubscribes = true;
        tvNoSubscribe = findViewById(R.id.tvNoSubscribe);
        tvNoSubscribe.setVisibility(View.GONE);

        subscribeButtons = findViewById(R.id.subscribeButtons);
        subscribeButtons.setVisibility(View.VISIBLE);

        bOk = findViewById(R.id.bOk);
        bOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMonthlySubscription || mYearlySubscription) {
                    setResult(SUBSCRIBE_RESULT_PREMIUM_OK_PRESSED);
                } else {
                    setResult(SUBSCRIBE_RESULT_FREE_OK_PRESSED);
                }
                finish();
            }
        });

        bBuyMonthly = findViewById(R.id.bBuyMonthly);
        bBuyMonthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mYearlySubscription) {
                    // If we already subscribed to yearly, launch replace flow
                    ArrayList<String> currentSubscriptionSku = new ArrayList<>();
                    currentSubscriptionSku.add(BillingConstants.SKU_YEARLY_SUBSCRIPTION);
                    mBillingManager.initiatePurchaseFlow(BillingConstants.SKU_MONTHLY_SUBSCRIPTION,
                            currentSubscriptionSku, BillingClient.SkuType.SUBS);
                } else {
                    mBillingManager.initiatePurchaseFlow(BillingConstants.SKU_MONTHLY_SUBSCRIPTION,
                            BillingClient.SkuType.SUBS);
                }
            }
        });
        bBuyYearly = findViewById(R.id.bBuyYearly);
        bBuyYearly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMonthlySubscription) {
                    // If we already subscribed to yearly, launch replace flow
                    ArrayList<String> currentSubscriptionSku = new ArrayList<>();
                    currentSubscriptionSku.add(BillingConstants.SKU_MONTHLY_SUBSCRIPTION);
                    mBillingManager.initiatePurchaseFlow(BillingConstants.SKU_YEARLY_SUBSCRIPTION,
                            currentSubscriptionSku, BillingClient.SkuType.SUBS);
                } else {
                    mBillingManager.initiatePurchaseFlow(BillingConstants.SKU_YEARLY_SUBSCRIPTION,
                            BillingClient.SkuType.SUBS);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBillingManager != null) {
            if (mBillingManager.getBillingClientResponseCode() == BillingClient.BillingResponse.OK) {
                mBillingManager.queryPurchases();
            }
            //if (mBillingManager.areSubscriptionsSupported())
            //    showNoSubscribes = false;
            //else
            //    showNoSubscribes = true;
        }
        updateUI();
    }

    @Override
    protected void onDestroy() {
        if (mBillingManager != null) {
            mBillingManager.destroy();
        }
        super.onDestroy();
    }

    private void updateUI() {
        if (showNoSubscribes) {
            tvNoSubscribe.setVisibility(View.VISIBLE);
            subscribeButtons.setVisibility(View.GONE);
//            if (showPayOrLogoffMessage) {
//                tvMessage.setVisibility(View.VISIBLE);
//            } else {
//                tvMessage.setVisibility(View.GONE);
//            }
        } else {
            tvNoSubscribe.setVisibility(View.GONE);
            subscribeButtons.setVisibility(View.VISIBLE);
            if (mMonthlySubscription) {
                bBuyMonthly.setText(R.string.subscribe_own);
                bBuyMonthly.setEnabled(false);

            } else {
                String text = mYearlySubscription
                        ? getString(R.string.subscribe_change) : getString(R.string.subscribe_buy);
                bBuyMonthly.setText(text);
                bBuyMonthly.setEnabled(true);
            }

            if (mYearlySubscription) {
                bBuyYearly.setText(R.string.subscribe_own);
                bBuyYearly.setEnabled(false);
            } else {
                String text = mMonthlySubscription
                        ? getString(R.string.subscribe_change) : getString(R.string.subscribe_buy);
                bBuyYearly.setText(text);
                bBuyYearly.setEnabled(true);

            }
        }

        if (showPayOrLogoffMessage && !(mYearlySubscription || mMonthlySubscription)) {
            tvMessage.setVisibility(View.VISIBLE);
        } else {
            tvMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBillingClientSetupFinished() {
        if (mBillingManager != null) {
            if (mBillingManager.getBillingClientResponseCode() == BillingClient.BillingResponse.OK) {
                mBillingManager.queryPurchases();
            }
            if (mBillingManager.areSubscriptionsSupported())
                showNoSubscribes = false;
            else
                showNoSubscribes = true;
        }
        updateUI();
    }

    @Override
    public void onConsumeFinished(String token, int result) {

    }

    @Override
    public void onPurchasesUpdated(List<Purchase> purchases) {
        mMonthlySubscription = false;
        mYearlySubscription = false;
        showNoSubscribes = false;

        for (Purchase purchase : purchases) {
            String endOfPaymentPeriod=null;
            switch (purchase.getSku()) {
                case BillingConstants.SKU_MONTHLY_SUBSCRIPTION:
                    mMonthlySubscription = true;
                    endOfPaymentPeriod = DateTime.addDays(
                            DateTime.convertLongToDate(purchase.getPurchaseTime()), 32);
                    break;
                case BillingConstants.SKU_YEARLY_SUBSCRIPTION:
                    mYearlySubscription = true;
                    endOfPaymentPeriod = DateTime.addDays(
                            DateTime.convertLongToDate(purchase.getPurchaseTime()), 367);
                    break;
            }
            if (endOfPaymentPeriod!=null)
                FBDatabase.getInstance().saveEndOfPrepaidPeriod(endOfPaymentPeriod);
        }

        updateUI();
    }

}
