package biz.atomeo.chronomy.util;

import android.util.Log;

import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.text.DateFormat;

import biz.atomeo.chronomy.R;

public class DateTime {
    private static final String TAG = "Chronomy/util/DateTime";

    private static String year1 = "";//.getString(R.string.year1);
    private static String year2 = "";//.getString(R.string.year2);
    private static String year3 = "лет";
    private static String month1 = "месяц";
    private static String month2 = "месяца";
    private static String month3 = "месяцев";
    private static String week1 = "неделя";
    private static String week2 = "недели";
    private static String week3 = "недель";
    private static String day1 = "день";
    private static String day2 = "дня";
    private static String day3 = "дней";
    private static String hour1 = "час";
    private static String hour2 = "часа";
    private static String hour3 = "часов";
    private static String minute1 = "минута";
    private static String minute2 = "минуты";
    private static String minute3 = "минут";
    private static String rightNow = "только что";
    private static String beforeEvent = "до события";
    private static String ago = "назад";
    private static boolean russian = false;

    public static void setup(String year1, String year2, String year3,
                             String month1, String month2,String month3,
                             String week1,String week2,String week3,
                             String day1,String day2,String day3,
                             String hour1,String hour2,String hour3,
                             String minute1,String minute2,String minute3,
                             String rightNow, String beforeEvent, String ago,
                             boolean russian) {
        DateTime.year1=year1;
        DateTime.year2=year2;
        DateTime.year3=year3;
        DateTime.month1=month1;
        DateTime.month2=month2;
        DateTime.month3=month3;
        DateTime.week1=week1;
        DateTime.week2=week2;
        DateTime.week3=week3;
        DateTime.day1=day1;
        DateTime.day2=day2;
        DateTime.day3=day3;
        DateTime.hour1=hour1;
        DateTime.hour2=hour2;
        DateTime.hour3=hour3;
        DateTime.minute1=minute1;
        DateTime.minute2=minute2;
        DateTime.minute3=minute3;
        DateTime.rightNow=rightNow;
        DateTime.beforeEvent=beforeEvent;
        DateTime.ago=ago;
        DateTime.russian=russian;
    }

    public static String dateShift(String someDate) {
        if (someDate==null) return "";
        try {

            org.joda.time.DateTime p1 = org.joda.time.DateTime.parse(someDate);
            org.joda.time.DateTime now = new org.joda.time.DateTime();


            Period period;
            boolean inFuture = false;
            if (p1.isBefore(now)) {
                period = new Period(p1, now, PeriodType.yearMonthDayTime());
            } else {
                inFuture = true;
                period = new Period(now, p1, PeriodType.yearMonthDayTime());
            }

            int type=1;
            switch (type) {
                case 0:
                    String rez;
                    //TODO: в jodatime есть Formatters, http://www.joda.org/joda-time/userguide.html#Formatters
                    if (period.getYears()>0) rez=conjuge(period.getYears(),year1,year2,year3);
                    else if (period.getMonths()>0) rez=conjuge(period.getMonths(),month1,month2,month3);
                    else if (period.getWeeks()>0) rez=conjuge(period.getWeeks(),week1,week2,week3);
                    else if (period.getDays()>0) rez= conjuge(period.getDays(),day1,day2,day3);
                    else if (period.getHours()>0) rez= conjuge(period.getHours(),hour1,hour2,hour3);
                    else if (period.getMinutes()>0) rez = conjuge(period.getMinutes(),minute1,minute2,minute3);
                    else {
                        rez=rightNow;
                        return rez;
                    }
                    if (inFuture) rez=rez+" "+beforeEvent;
                    else rez=rez+" "+ago;
                    return rez;//period.toString();
                case 1:
                    StringBuffer r = new StringBuffer();
                    int typed=0;
                    int wantToType=2;
                    typed=appendConjudeIfSignificant(period.getYears(),year1,year2,year3,r,typed,wantToType);
                    typed=appendConjudeIfSignificant(period.getMonths(), month1,month2,month3,r,typed,wantToType);
                    typed=appendConjudeIfSignificant(period.getWeeks(), week1,week2,week3,r,typed,wantToType);
                    typed=appendConjudeIfSignificant(period.getDays(), day1,day2,day3,r,typed,wantToType);
                    typed=appendConjudeIfSignificant(period.getHours(), hour1,hour2,hour3,r,typed,wantToType);
                    typed=appendConjudeIfSignificant(period.getMinutes(), minute1,minute2,minute3,r,typed,wantToType);
                    if (typed==0) {
                        r.append(rightNow);
                    } else {
                        if (inFuture) {
                            r.append(" ");
                            r.append(beforeEvent);
                        }
                        else {
                            r.append(" ");
                            r.append(ago);
                        }
                    }
                    return r.toString();
            }
        } catch (Exception e) {
            //TODO:
        }
        return someDate;
    }

    private static int appendConjudeIfSignificant(int num, String one, String two, String three, StringBuffer target, int typed, int wantToType) {
        if (typed<wantToType) {
            if (num>0) {
                typed++;
                if (typed>1) target.append(", ");
                target.append(conjuge(num, one, two, three));
            } else {
                if (typed>0) typed++;
            }
        }
        return typed;
    }

    //склоняю строку в зависимоти от числа (выбираю правильный вариант)
    //TODO: потенциально создаю кучу мелких переменных, переделать???
    private static String conjuge(int num, String one, String two, String three) {
        String rez=Long.toString(num)+"\u00A0";
        if (russian) {
            int ost = num % 10;

            if ((num >= 11) && (num <= 14)) return rez + three;
            if (ost == 1) return rez + one;
            if (ost >= 2 && ost <= 4) return rez + two;
            return rez + three;
        } else {
            if (num == 1) return rez + one;
            else return rez + two;
        }
    }

    public static String shortDate(String someDate) {
        if (someDate==null || someDate.length()==0) return "";
        try {
            org.joda.time.DateTime p1 = org.joda.time.DateTime.parse(someDate);
            return org.joda.time.format.DateTimeFormat.shortDateTime().print(p1);
        } catch (Exception e) {
            Log.d(TAG,"Error in date parsing:"+someDate);
            return "";
        }
    }

    public static String convertLongToDate(long time) {
        return (new org.joda.time.DateTime(time)).toString();
    }

    public static String addDays(String somedate, int days) {
        try {
            return org.joda.time.DateTime.parse(somedate).plusDays(days).toString();
        } catch (Exception e) {
            Log.d(TAG,"addDays: Error in date parsing:"+somedate);
            return "";
        }
    }

    public static boolean isDateInFuture(String someDate) {
        try {
            org.joda.time.DateTime p1 = org.joda.time.DateTime.parse(someDate);
            return p1.isAfterNow();
        } catch (Exception e) {
            return false;
        }
    }
}
